---
title: "Utilisation d'Ara pour logguer l'ensemble des déploiements Ansible d'AWX."
date: 2022-08-09T11:39:08
description: "Utilisation d'Ara pour logguer l'ensemble des déploiement Ansible d'AWX."
thumbnailImagePosition: left
thumbnailImage: https://raw.githubusercontent.com/ansible/awx-logos/master/awx/ui/client/assets/logo-login.svg
tags: 
  - "Ara"
  - "Ansible"
  - "CI/CD"
  - "Kaniko"
  - "Kubernetes"
---

Ici nous allons voir comment lier l'outil *ARA* et *AWX* sur une plateforme Kubernetes de manière **simple**.
<!--more-->
Postula de départ : 

- Je suis sur un cluster Kubernetes managé par CIVO
- Le namespace de référence : awx
- Cette présentation est un PoC / Démo, toutes les “bonnes pratiques” ne sont/seront pas appliquées. Ici, le but étant d’arriver au résultat demandé. Des améliorations seront à apporter.
---

{{< toc >}}

---
## Step 1 : Installation d’AWX Operator sur Kubernetes et première instance

### Installation de l’operator :

La première étape consiste à monter un AWX, sur mon cluster **Kubernetes.** pour cela nous allons suivre pas-à-pas la documentation d’AWX sur l’installation de l’opérateur. Cette documentation se trouve sur cette page : [https://github.com/ansible/awx-operator#helm-install-on-existing-cluster](https://github.com/ansible/awx-operator#helm-install-on-existing-cluster).

Nous allons utiliser ***Helm*** pour installer l’opérateur AWX avec le fichier de valeurs suivant :

```yaml
#awxval.yaml
AWX:
  enabled: true
  name: awx # oui je n'ai pas envie de me prendre la tête :-)
  # postgres:
  #   dbName: Unset
  #   enabled: false
  #   host: Unset
  #   password: Unset
  #   port: 5678
  #   sslmode: prefer
  #   type: unmanaged
  #   username: admin
  spec:
    admin_user: admin 
```

En effet je n’ai pas, pour ce *PoC/Démo*, voulu définir de base de données externe pour AWX, **attention, cela est une mauvaise pratique, je vous encourage vivement à le faire**.

On ajoute le repo et on *installe* AWX avec le fichier de valeurs : *awxval.yaml*

```bash
$ helm repo add awx-operator https://ansible.github.io/awx-operator/
$ helm repo update

```

On vérifie que le *charts* a bien été ajouté :

![charts](/images/awx-and-ara/charts.png)

*ou*

```bash
$ helm repo list | grep -i awx
awx-operator     	https://ansible.github.io/awx-operator/
```

On lance la création de *l’operator* :

```bash
$ helm install -n awx --create-namespace awx-operator awx-operator/awx-operator -f awxval.yaml                                                                                                        130 ↵
NAME: awx-operator
LAST DEPLOYED: Mon Aug  X XX:XX:XX 2022
NAMESPACE: awx
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
AWX Operator installed with Helm Chart version 0.25.0
```

Ci-après le résultat : 

![charts](/images/awx-and-ara/deployment.png)

*ou*

```bash
$ kubectl get all                                                                                                                                                                                           130 ↵
NAME                                                  READY   STATUS    RESTARTS   AGE
pod/awx-operator-controller-manager-645765798-xdb7w   2/2     Running   0          10m

NAME                                                      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/ara-http                                          ClusterIP   10.43.194.108   <none>        8000/TCP   149m
service/awx-operator-controller-manager-metrics-service   ClusterIP   10.43.223.7     <none>        8443/TCP   10m

NAME                                              READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/awx-operator-controller-manager   1/1     1            1           10m

NAME                                                        DESIRED   CURRENT   READY   AGE
replicaset.apps/awx-operator-controller-manager-645765798   1         1         1       
```

Petite pause explication, ici nous avons déployé l’opérateur qui nous permettra de créer 1 ou *N* instances AWX (pour info ils utilisent ansible pour créer les instances, nous le verrons par la suite ;-D )

### Lançons notre première instance *AWX*

Ici, aussi, je vais utiliser les exemples fournis par AWX pour créer une instance.

Regardons et commentons l’exemple fourni (adresse : [https://raw.githubusercontent.com/ansible/awx-operator/devel/config/samples/awx_v1beta1_awx.yaml](https://raw.githubusercontent.com/ansible/awx-operator/devel/config/samples/awx_v1beta1_awx.yaml)) :

```yaml
#
---
apiVersion: awx.ansible.com/v1beta1
kind: AWX
metadata:
# Je vais changer le nom
# name: example-awx
  name: thunder-awx # Ac/Dc - Atlantic City / Descente Cataclysmique -- pour ceux qui ont la ref ;)
# Ici je vais ajouter le namespace qui est associé
  namespace: awx
spec:
# cette partie est dédiée pour la partie UI d'AWX
  web_resource_requirements: # simple requete de demande minimum de ressources pour la partie web sans limites
    requests:
      cpu: 50m
      memory: 128M
# cette partie Orchestre les tâches à effectuer par AWX
  task_resource_requirements:
    requests:
      cpu: 50m
      memory: 128M
# cette dernière sera l'élément à partir desquelles les tâches s'executeront
  ee_resource_requirements:
    requests:
      cpu: 50m
      memory: 64M
```
Je vous conseille, toutefois, d'augmenter les seuils minimum de *request*.

```yaml
$ kubectl apply -f awx_v1beta1_awx.yaml
awx.awx.ansible.com/thunder-awx created
```

**L’installation prend quelques secondes à quelques minutes.**

Exemple de log de l’opérateur. Comme vous pouvez le voir l’installation se fait via ansible 😄 :

```bash
$ kubectl logs -f awx-operator-controller-manager-645765798-xdb7w -c awx-manager                                                                                                                            130 ↵
{"level":"info","ts":1659973554.5564585,"logger":"cmd","msg":"Version","Go Version":"go1.16.9","GOOS":"linux","GOARCH":"amd64","ansible-operator":"v1.12.0","commit":"d3b2761afdb78f629a7eaf4461b0fb8ae3b02860"}
{"level":"info","ts":1659973554.55681,"logger":"cmd","msg":"Watching single namespace.","Namespace":"awx"}
{"level":"info","ts":1659973555.369485,"logger":"controller-runtime.metrics","msg":"metrics server is starting to listen","addr":"127.0.0.1:8080"}
{"level":"info","ts":1659973555.3708863,"logger":"watches","msg":"Environment variable not set; using default value","envVar":"ANSIBLE_VERBOSITY_AWX_AWX_ANSIBLE_COM","default":2}
{"level":"info","ts":1659973555.371074,"logger":"watches","msg":"Environment variable not set; using default value","envVar":"ANSIBLE_VERBOSITY_AWXBACKUP_AWX_ANSIBLE_COM","default":2}
{"level":"info","ts":1659973555.3711421,"logger":"watches","msg":"Environment variable not set; using default value","envVar":"ANSIBLE_VERBOSITY_AWXRESTORE_AWX_ANSIBLE_COM","default":2}
{"level":"info","ts":1659973555.3712292,"logger":"ansible-controller","msg":"Watching resource","Options.Group":"awx.ansible.com","Options.Version":"v1beta1","Options.Kind":"AWX"}
{"level":"info","ts":1659973555.37154,"logger":"ansible-controller","msg":"Watching resource","Options.Group":"awx.ansible.com","Options.Version":"v1beta1","Options.Kind":"AWXBackup"}
{"level":"info","ts":1659973555.371651,"logger":"ansible-controller","msg":"Watching resource","Options.Group":"awx.ansible.com","Options.Version":"v1beta1","Options.Kind":"AWXRestore"}
{"level":"info","ts":1659973555.3732014,"logger":"proxy","msg":"Starting to serve","Address":"127.0.0.1:8888"}
{"level":"info","ts":1659973555.3738666,"logger":"controller-runtime.manager","msg":"starting metrics server","path":"/metrics"}
I0808 15:45:55.373854       7 leaderelection.go:243] attempting to acquire leader lease awx/awx-operator...
I0808 15:45:55.386731       7 leaderelection.go:253] successfully acquired lease awx/awx-operator
{"level":"info","ts":1659973555.3870814,"logger":"controller-runtime.manager.controller.awx-controller","msg":"Starting EventSource","source":"kind source: awx.ansible.com/v1beta1, Kind=AWX"}
{"level":"info","ts":1659973555.3871539,"logger":"controller-runtime.manager.controller.awx-controller","msg":"Starting Controller"}
{"level":"info","ts":1659973555.3872771,"logger":"controller-runtime.manager.controller.awxbackup-controller","msg":"Starting EventSource","source":"kind source: awx.ansible.com/v1beta1, Kind=AWXBackup"}
{"level":"info","ts":1659973555.387425,"logger":"controller-runtime.manager.controller.awxbackup-controller","msg":"Starting Controller"}
{"level":"info","ts":1659973555.3876975,"logger":"controller-runtime.manager.controller.awxrestore-controller","msg":"Starting EventSource","source":"kind source: awx.ansible.com/v1beta1, Kind=AWXRestore"}
{"level":"info","ts":1659973555.3877542,"logger":"controller-runtime.manager.controller.awxrestore-controller","msg":"Starting Controller"}
{"level":"info","ts":1659973555.4884973,"logger":"controller-runtime.manager.controller.awx-controller","msg":"Starting workers","worker count":4}
{"level":"info","ts":1659973555.4885561,"logger":"controller-runtime.manager.controller.awxbackup-controller","msg":"Starting workers","worker count":4}
{"level":"info","ts":1659973555.4886672,"logger":"controller-runtime.manager.controller.awxrestore-controller","msg":"Starting workers","worker count":4}

--------------------------- Ansible Task StdOut -------------------------------

TASK [installer : Check for presence of Deployment] ****************************
task path: /opt/ansible/roles/installer/tasks/main.yml:2

-------------------------------------------------------------------------------
{"level":"info","ts":1659975727.372163,"logger":"logging_event_handler","msg":"[playbook task start]","name":"thunder-awx","namespace":"awx","gvk":"awx.ansible.com/v1beta1, Kind=AWX","event_type":"playbook_on_task_start","job":"5397096743336549648","EventData.Name":"installer : Check for presence of Deployment"}
{"level":"info","ts":1659975729.4608185,"logger":"proxy","msg":"Cache miss: apps/v1, Kind=Deployment, awx/thunder-awx"}

--------------------------- Ansible Task StdOut -------------------------------

TASK [installer : Start installation] ******************************************
task path: /opt/ansible/roles/installer/tasks/main.yml:11

-------------------------------------------------------------------------------
{"level":"info","ts":1659975729.5933785,"logger":"logging_event_handler","msg":"[playbook task start]","name":"thunder-awx","namespace":"awx","gvk":"awx.ansible.com/v1beta1, Kind=AWX","event_type":"playbook_on_task_start","job":"5397096743336549648","EventData.Name":"installer : Start installation"}

--------------------------- Ansible Task StdOut -------------------------------

TASK [installer : Patching labels to AWX kind] *********************************
task path: /opt/ansible/roles/installer/tasks/install.yml:2

-------------------------------------------------------------------------------
{"level":"info","ts":1659975729.7257488,"logger":"logging_event_handler","msg":"[playbook task start]","name":"thunder-awx","namespace":"awx","gvk":"awx.ansible.com/v1beta1, Kind=AWX","event_type":"playbook_on_task_start","job":"5397096743336549648","EventData.Name":"installer : Patching labels to AWX kind"}
{"level":"info","ts":1659975730.7625737,"logger":"proxy","msg":"Read object from cache","resource":{"IsResourceRequest":true,"Path":"/apis/awx.ansible.com/v1beta1/namespaces/awx/awxs/thunder-awx","Verb":"get","APIPrefix":"apis","APIGroup":"awx.ansible.com","APIVersion":"v1beta1","Namespace":"awx","Resource":"awxs","Subresource":"","Name":"thunder-awx","Parts":["awxs","thunder-awx"]}}

--------------------------- Ansible Task StdOut -------------------------------

TASK [installer : Include secret key configuration tasks] **********************
task path: /opt/ansible/roles/installer/tasks/install.yml:20

-------------------------------------------------------------------------------
{"level":"info","ts":1659975730.938849,"logger":"logging_event_handler","msg":"[playbook task start]","name":"thunder-awx","namespace":"awx","gvk":"awx.ansible.com/v1beta1, Kind=AWX","event_type":"playbook_on_task_start","job":"5397096743336549648","EventData.Name":"installer : Include secret key configuration tasks"}
{"level":"info","ts":1659975730.986549,"logger":"logging_event_handler","msg":"[playbook task start]","name":"thunder-awx","namespace":"awx","gvk":"awx.ansible.com/v1beta1, Kind=AWX","event_type":"playbook_on_task_start","job":"5397096743336549648","EventData.Name":"installer : Check for specified secret key configuration"}

--------------------------- Ansible Task StdOut -------------------------------

TASK [installer : Check for specified secret key configuration] ****************
task path: /opt/ansible/roles/installer/tasks/secret_key_configuration.yml:2

-------------------------------------------------------------------------------
{"level":"info","ts":1659975731.0852208,"logger":"logging_event_handler","msg":"[playbook task start]","name":"thunder-awx","namespace":"awx","gvk":"awx.ansible.com/v1beta1, Kind=AWX","event_type":"playbook_on_task_start","job":"5397096743336549648","EventData.Name":"installer : Check for default secret key configuration"}

--------------------------- Ansible Task StdOut -------------------------------

TASK [installer : Check for default secret key configuration] ******************
task path: /opt/ansible/roles/installer/tasks/secret_key_configuration.yml:11

-------------------------------------------------------------------------------
{"level":"info","ts":1659975732.0479636,"logger":"proxy","msg":"Cache miss: /v1, Kind=Secret, awx/thunder-awx-secret-key"}
{"level":"info","ts":1659975732.2664962,"logger":"logging_event_handler","msg":"[playbook task start]","name":"thunder-awx","namespace":"awx","gvk":"awx.ansible.com/v1beta1, Kind=AWX","event_type":"playbook_on_task_start","job":"5397096743336549648","EventData.Name":"installer : Create secret key secret"}

--------------------------- Ansible Task StdOut -------------------------------

TASK [installer : Create secret key secret] ************************************
task path: /opt/ansible/roles/installer/tasks/secret_key_configuration.yml:25

-------------------------------------------------------------------------------
{"level":"info","ts":1659975733.0719173,"logger":"proxy","msg":"Cache miss: /v1, Kind=Secret, awx/thunder-awx-secret-key"}
{"level":"info","ts":1659975733.0775547,"logger":"proxy","msg":"Cache miss: /v1, Kind=Secret, awx/thunder-awx-secret-key"}
{"level":"info","ts":1659975733.088761,"logger":"proxy","msg":"Injecting owner reference"}
{"level":"info","ts":1659975733.0898829,"logger":"proxy","msg":"Watching child resource","kind":"/v1, Kind=Secret","enqueue_kind":"awx.ansible.com/v1beta1, Kind=AWX"}
{"level":"info","ts":1659975733.0900571,"logger":"controller-runtime.manager.controller.awx-controller","msg":"Starting EventSource","source":"kind source: /v1, Kind=Secret"}

--------------------------- Ansible Task StdOut -------------------------------

TASK [installer : Read secret key secret] **************************************
task path: /opt/ansible/roles/installer/tasks/secret_key_configuration.yml:31

-------------------------------------------------------------------------------
{"level":"info","ts":1659975733.2136488,"logger":"logging_event_handler","msg":"[playbook task start]","name":"thunder-awx","namespace":"awx","gvk":"awx.ansible.com/v1beta1, Kind=AWX","event_type":"playbook_on_task_start","job":"5397096743336549648","EventData.Name":"installer : Read secret key secret"}
{"level":"info","ts":1659975733.9536996,"logger":"proxy","msg":"Read object from cache","resource":{"IsResourceRequest":true,"Path":"/api/v1/namespaces/awx/secrets/thunder-awx-secret-key","Verb":"get","APIPrefix":"api","APIGroup":"","APIVersion":"v1","Namespace":"awx","Resource":"secrets","Subresource":"","Name":"thunder-awx-secret-key","Parts":["secrets","thunder-awx-secret-key"]}}
{"level":"info","ts":1659975734.2683332,"logger":"logging_event_handler","msg":"[playbook task start]","name":"thunder-awx","namespace":"awx","gvk":"awx.ansible.com/v1beta1, Kind=AWX","event_type":"playbook_on_task_start","job":"5397096743336549648","EventData.Name":"installer : Load LDAP CAcert certificate"}

--------------------------- Ansible Task StdOut -------------------------------
....
--------------------------- Ansible Task StdOut -------------------------------

 TASK [Remove ownerReferences reference] ******************************** 
ok: [localhost] => (item=None) => {"censored": "the output has been hidden due to the fact that 'no_log: true' was specified for this result", "changed": false}

-------------------------------------------------------------------------------
```

L'installation s'est déroulée correctement, voyons ce qu’il a pu installer ?

```bash
$ kubectl get all                                                                                                                                                                                           130 ↵
NAME                                                  READY   STATUS    RESTARTS   AGE
pod/awx-operator-controller-manager-645765798-xdb7w   2/2     Running   0          44m
pod/thunder-awx-postgres-0                            1/1     Running   0          8m6s
pod/thunder-awx-5757c69dfb-llsgq                      4/4     Running   0          6m54s

NAME                                                      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/ara-http                                          ClusterIP   10.43.194.108   <none>        8000/TCP   3h3m
service/awx-operator-controller-manager-metrics-service   ClusterIP   10.43.223.7     <none>        8443/TCP   44m
service/thunder-awx-postgres                              ClusterIP   None            <none>        5432/TCP   8m7s
service/thunder-awx-service                               ClusterIP   10.43.155.123   <none>        80/TCP     6m58s

NAME                                              READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/awx-operator-controller-manager   1/1     1            1           44m
deployment.apps/thunder-awx                       1/1     1            1           6m55s

NAME                                                        DESIRED   CURRENT   READY   AGE
replicaset.apps/awx-operator-controller-manager-645765798   1         1         1       44m
replicaset.apps/thunder-awx-5757c69dfb                      1         1         1       6m55s

NAME                                    READY   AGE
statefulset.apps/thunder-awx-postgres   1/1     8m7s

---

$ kubectl get secrets,cm                                            
NAME                                                 TYPE                                  DATA   AGE
secret/awx-operator-controller-manager-token-z2r4s   kubernetes.io/service-account-token   3      50m
secret/sh.helm.release.v1.awx-operator.v1            helm.sh/release.v1                    1      50m
secret/thunder-awx-app-credentials                   Opaque                                3      12m
secret/thunder-awx-token-jtcr5                       kubernetes.io/service-account-token   3      12m
*secret/thunder-awx-admin-password                    Opaque                                1      13m*
secret/thunder-awx-secret-key                        Opaque                                1      13m
*secret/thunder-awx-postgres-configuration            Opaque                                6      13m*
secret/thunder-awx-broadcast-websocket               Opaque                                1      13m

NAME                                        DATA   AGE
configmap/awx-operator-awx-manager-config   1      50m
*configmap/thunder-awx-awx-configmap         5      12m*
configmap/awx-operator                      0      49m
```

Ok, quels sont les *ConfigMap* et les *Secrets* qu'il a créés ? Regardons :

- *configmap/awx-operator-awx-manager-config*
 
  ![cm_manager](/images/awx-and-ara/cm_awx.png)
    
- *secret/thunder-awx-admin-password*
    
    Hey, je ne vais quand même pas vous montrer le mot de passe hein ?! ;-)
    
  ![secret_admin_pwd](/images/awx-and-ara/secret_admin_pwd.png)
    
- *secret/thunder-awx-postgres-configuration*
    
  ![secret_thunder_confing](/images/awx-and-ara/secret_thunder_confing.png)
    

Tout est en place pour y accéder maintenant, let’s go!

### Connexion!

On se connecte avec le user *admin* ainsi que le password qui se trouve dans le secret *secret/thunder-awx-admin-password* :

![welcome_awx](/images/awx-and-ara/welcome-awx.png)

Après la connexion la page about ;-)

![about_awx](/images/awx-and-ara/about_awx.png)

Passons à la création du déploiement pour Ara, nous reviendrons sur la partie AWX un peu plus tard.

## Step 2 : Mise en place d’ARA sur Kubernetes

Ici, le but de cette mise en place d’Ara sur Kube est de créer un serveur qui va récupérer tous les logs 

### Qu’est ce qu'ARA ?

Ara permet de fournir un rapport d'exécution de playbooks *Ansible* consultable soit en ligne de commande soit via une interface graphique.
La mise en place du plugin de callback *Ansible* et du serveur permet de consulter l'ensemble des exécutions *Ansible*

Exemple provenant du Github d'[Ara](https://github.com/ansible-community/ara)  
Interface Web :  
![Gui](https://github.com/ansible-community/ara/blob/master/doc/source/_static/ui-web-demo.gif?raw=true)  
Ligne de commande :  
![Command Line](https://github.com/ansible-community/ara/blob/master/doc/source/_static/getting-started.gif?raw=true)  

### Création du *configMap*

Commençons par créer une image temporaire afin de récupérer le fichier de configuration d’Ara

```bash
$ kubectl run ara-server-tmp --image recordsansible/ara-api:latest 
$ kubectl cp ara-server-tmp:/opt/ara/settings.yaml settings.yaml
$ # Modifions le ficher *settings.yaml*
$ vim settings.yaml 
$ kubectl create cm ara-config --from-file settings.yaml
# pour améliorer il faudrait 
# - mettre les variables d'env *password* en secret
# -- https://ara.readthedocs.io/en/latest/api-configuration.html?highlight=DATABASE_PASSWORD#ara-database-password
# -- https://ara.readthedocs.io/en/latest/api-configuration.html?highlight=DATABASE_PASSWORD#ara-secret-key
$ kubectl get cm
```

Nous sommes à l’étape `vim settings.yaml` et voici les modifications que j’ai réalisées sur le fichier ainsi que les commentaires associés

```yaml
# settings.yaml
---
# This is a default settings template generated by ARA.
# To use a settings file such as this one, you need to export the
# ARA_SETTINGS environment variable like so:
#   $ export ARA_SETTINGS="/opt/ara/settings.yaml"

default:
  ALLOWED_HOSTS:
# **SAISISSEZ** le nom du service du/des pods (il sera important pour après)
  - ara-http
  - localhost
  BASE_DIR: /opt/ara
# A modifier
  CORS_ORIGIN_ALLOW_ALL: true
  CORS_ORIGIN_REGEX_WHITELIST: []
# A modifier
  CORS_ORIGIN_WHITELIST: []
  CSRF_TRUSTED_ORIGINS: []
  DATABASE_CONN_MAX_AGE: 0
# Valeurs disponibles : [django.db.backends.sqlite3,django.db.backends.postgresql,django.db.backends.mysql,ara.server.db.backends.distributed_sqlite]
  DATABASE_ENGINE: django.db.backends.sqlite3
  DATABASE_HOST: null
# changez l'endroit où sera stockée la bdd si vous utilisez sqlite
# puisqu'initialement la base sqlite se trouve dans le même répertoire que le settings.yaml
  DATABASE_NAME: /opt/ansible.sqlite
  DATABASE_OPTIONS: {}
# A modifier si vous utilisez autre chose que sqlite
  *DATABASE_PASSWORD: null*
  DATABASE_PORT: null
  DATABASE_USER: null
# Pratique si vous avez un dysfonctionnement avec l'API
  DEBUG: false
  DISTRIBUTED_SQLITE: false
  DISTRIBUTED_SQLITE_PREFIX: ara-report
  DISTRIBUTED_SQLITE_ROOT: /var/www/logs
  EXTERNAL_AUTH: false
  LOGGING:
    disable_existing_loggers: false
    formatters:
      normal:
        format: '%(asctime)s %(levelname)s %(name)s: %(message)s'
    handlers:
      console:
        class: logging.StreamHandler
        formatter: normal
        level: INFO
        stream: ext://sys.stdout
    loggers:
      ara:
        handlers:
        - console
        level: INFO
        propagate: 0
    version: 1
# A modifier si vous souhaitez débugger
  LOG_LEVEL: INFO
  PAGE_SIZE: 100
  READ_LOGIN_REQUIRED: false
# A modifier si vous souhaitez gérer vous même le secret
  SECRET_KEY: sOQl2kCrm4xKz8qh5qKGF7HJ0rjz4pkYmCKVyuVFs6xSGT6Ada
  TIME_ZONE: Etc/UTC
  WRITE_LOGIN_REQUIRED: false
```

### Création du *deployment*

Avant de commencer à écrire/générer le manifest de *deployment* Kubernetes je vous rappelle que ceci est une démo/PoC. Je vous enjoins à revoir le postula de départ.

```yaml
# ara-deployment.yml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ara-server
  namespace: awx
  labels:
    app: ara-server
  annotations:
    deployment.kubernetes.io/revision: '2'
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ara-server
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: ara-server
    spec:
      containers:
        - name: ara-api
          image: recordsansible/ara-api:latest
          volumeMounts:
          - name: config
            mountPath: /opt/ara/
          ports:
            - containerPort: 8000
              protocol: TCP
          resources: {}
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          imagePullPolicy: Always
      restartPolicy: Always
      terminationGracePeriodSeconds: 30
      dnsPolicy: ClusterFirst
      securityContext: {}
      schedulerName: default-scheduler
      volumes:
      - name: config
        configMap:
          name: ara-config
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 25%
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600

# ara-service.yml
---
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: ara-server
# Vous vous rappelez du nom de l'ALLOWED_HOST dans le fichier de config, 
# c'est le nom du service qui fait référence.
  name: ara-http
  namespace: awx
spec:
  ports:
  - port: 8000
    protocol: TCP
    targetPort: 8000
  selector:
    app: ara-server
status:
  loadBalancer: {}
```

Une fois déployé, via un `kubectl apply/create`, vous devriez obtenir ceci :

```bash
$ kubectl get po,svc,deploy,cm                                                                                       
NAME                                                  READY   STATUS    RESTARTS   AGE
pod/awx-operator-controller-manager-645765798-xdb7w   2/2     Running   0          15h
pod/thunder-awx-postgres-0                            1/1     Running   0          15h
pod/thunder-awx-5757c69dfb-llsgq                      4/4     Running   0          14h
pod/ara-server-6d9f9fbd-jh442                         1/1     Running   0          6m40s

NAME                                                      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/ara-http                                          ClusterIP   10.43.194.108   <none>        8000/TCP   17h
service/awx-operator-controller-manager-metrics-service   ClusterIP   10.43.223.7     <none>        8443/TCP   15h
service/thunder-awx-postgres                              ClusterIP   None            <none>        5432/TCP   15h
service/thunder-awx-service                               ClusterIP   10.43.155.123   <none>        80/TCP     14h

NAME                                              READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/awx-operator-controller-manager   1/1     1            1           15h
deployment.apps/thunder-awx                       1/1     1            1           14h
deployment.apps/ara-server                        1/1     1            1           6m40s

NAME                                        DATA   AGE
configmap/kube-root-ca.crt                  1      6d22h
configmap/awx-operator-awx-manager-config   1      15h
configmap/thunder-awx-awx-configmap         5      14h
configmap/ara-config                        1      8m55s
configmap/awx-operator                      0      15h
```

/!\ Avant de passer à la modification d’AWX et à la création d’un environnement d’exécution, nous devons nous assurer que le serveur ARA récupère bien les *outputs* des playbooks *Ansible*.

### Validation du Fonctionnement

Ici nous allons déployer un simple pod **temporaire**, avec un playbook simple pour valider la remontée des infos dans Ara.

```bash
$ kubectl run ansible-tmp --image python:latest -it --rm --restart Never -- /bin/bash                                                                     
root@ansible-tmp:/# pip3 install ara ansible
root@ansible-tmp:/# export ARA_API_CLIENT="http"
# ci-dessous on ajoute le nom du service.
root@ansible-tmp:/# export ARA_API_SERVER="http://ara-http:8000"
root@ansible-tmp:/# export ANSIBLE_CALLBACK_PLUGINS="$(python3 -m ara.setup.callback_plugins)"
root@ansible-tmp:/# cat << EOF >> play.yml
> - hosts: localhost
>   gather_facts: false
>   tasks:
>   - setup:
> EOF
root@ansible-tmp:/# cat play.yml 
- hosts: localhost
  gather_facts: false
  tasks:
  - setup:
root@ansible-tmp:/# ansible-playbook play.yml 
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

PLAY [localhost] **************************************************************************************************************************************************************************************************

TASK [setup] ******************************************************************************************************************************************************************************************************
ok: [localhost]

PLAY RECAP ********************************************************************************************************************************************************************************************************
localhost                  : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

root@ansible-tmp:/# echo $ANSIBLE_CALLBACK_PLUGINS
/usr/local/lib/python3.10/site-packages/ara/plugins/callback
root@ansible-tmp:/#
```

Notez la valeur de `echo $ANSIBLE_CALLBACK_PLUGINS` elle vous servira dans AWX.

Allons vérifier en faisant un petit forward de port sur le service ***ara-http***

![ara_test_1](/images/awx-and-ara/ara_test_1.png)

Comme on peut le voir le resultat du playbook est bien remonté dans Ara.  
Détaillons le résultat : 

![ara_test_2](/images/awx-and-ara/ara_test_2.png)

A ce stade nous avons terminé la configuration, minimal, d’Ara. Maintenant passons à la partie la plus complexe… enfin pour ma part : créer un environnement d’exécution pour AWX.  
Les environnements d’exécution seront créés sur **Gitlab** avec un **runner** interne. L’image des containers sera mis à disposition sur mon registry interne de **Gitlab**. 

A adapter en fonction de vos besoins. ;-)

## Step 3 : Awx et l’EE

### Utilisation de Gitlab pour générer l’image de l’EE

Allez c’est parti ! 

Pour cette partie nous n’allons pas installer ansible-builder sur notre machine, nous allons utiliser plusieurs containers pour builder l’image EE. Pour cela la pipeline CI/CD de **Gitlab** sera mise à contribution pour fabriquer l'image de L'EE. 

Ci-après les fichiers utilisés par *ansible-builder* pour générer le context et builder l’image par la suite :

```yaml
# execution-environment.yml
---
version: 1
dependencies:
    python: requirements.txt
additional_build_steps:
  append:
    - RUN pip install ansible

# requirements.txt
---
ara==1.5.8
ansible
```

Ci-dessous lae pipeline CI utiliser pour générer l’EE :

```yaml
# .gitlab-ci.yaml
--- 
variables:
    CUSTOM_TAG: ${CI_COMMIT_SHA}
    DOCKER_CONTEXT_PATH: "context"

stages:
    - init
    - build

# Description : Ici on génère le context de création de l'environnement d'exécution (1)
ansible_builder:
    stage: init
    image: python:3.9.13-slim-buster
    artifacts:
        untracked: false
        expire_in: 2 days
        paths:
            - "context"
    before_script:
        - pip install ansible-builder
    script:
        - ansible-builder create -f execution-environment.yml

# Description : A partir du *context* nous générons l'image de l'EE et publions sur le registry interne de gitlab
# Librement inspiré de r2devops : https://r2devops.io/_/r2devops-bot/docker_build
image_build:
  stage: build
  dependencies:
    - "ansible_builder"
  image:
    name: gcr.io/kaniko-project/executor:${IMAGE_TAG}
    entrypoint: [""]
  variables:
    CUSTOM_REGISTRIES_DESTINATIONS: ""
    CONFIG_FILE: ""
    COMMIT_CREATE_LATEST: "false"
    TAG_CREATE_LATEST: "true"
    KANIKO_USE_NEWRUN: "true"
    DOCKERFILE_PATH: "Dockerfile"
    DOCKER_USE_CACHE: "false"
    DOCKER_CACHE_TTL: "336h"
    DOCKER_VERBOSITY: "info"
    DOCKER_OPTIONS: ""
    IMAGE_TAG: "v1.5.1-debug"
  script:
    - mkdir -p /kaniko/.docker/

    - if [ ! -z ${CI_COMMIT_TAG} ]; then
    -   IMAGE_TAG=${CI_COMMIT_TAG}
    -   if [ ${TAG_CREATE_LATEST} == "true" ]; then
    -     OPTIONAL_TAG="--destination ${CI_REGISTRY_IMAGE}:latest"
    -   fi
    - else
    -   IMAGE_TAG=${CI_COMMIT_SHA}
    -   if [ ${COMMIT_CREATE_LATEST} == "true" ]; then
    -     OPTIONAL_TAG="--destination ${CI_REGISTRY_IMAGE}:latest"
    -   fi
    - fi

    - if [ "${DOCKER_USE_CACHE}" = "true" ]; then
    -   DOCKER_OPTIONS="--cache=true --cache-ttl=${DOCKER_CACHE_TTL} ${DOCKER_OPTIONS}"
    - fi

    - if [ "${KANIKO_USE_NEWRUN}" = "true" ]; then
    -   DOCKER_OPTIONS="--use-new-run ${DOCKER_OPTIONS}"
    - fi

    - DOCKER_CONTEXT_PATH=${CI_PROJECT_DIR}/${DOCKER_CONTEXT_PATH}

    - DOCKER_OPTIONS="--verbosity=${DOCKER_VERBOSITY} ${DOCKER_OPTIONS}"

    - if [ ! -z "$CUSTOM_REGISTRIES_DESTINATIONS" ]; then
    -   cp -rf $CONFIG_FILE /kaniko/.docker/config.json
    -   DOCKER_OPTIONS="${DOCKER_OPTIONS} $CUSTOM_REGISTRIES_DESTINATIONS"
    -   /kaniko/executor --context ${DOCKER_CONTEXT_PATH} --dockerfile ${DOCKER_CONTEXT_PATH}/${DOCKERFILE_PATH} ${DOCKER_OPTIONS}
    - else
    -   echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    -     if [ ! -z ${CUSTOM_TAG} ]; then
    -       /kaniko/executor --context ${DOCKER_CONTEXT_PATH} --dockerfile ${DOCKER_CONTEXT_PATH}/${DOCKERFILE_PATH} --destination ${CI_REGISTRY_IMAGE}:${CUSTOM_TAG} ${DOCKER_OPTIONS}
    -   echo "CUSTOM_TAG:${CUSTOM_TAG}"
    -     else
    -       /kaniko/executor --context ${DOCKER_CONTEXT_PATH} --dockerfile ${DOCKER_CONTEXT_PATH}/${DOCKERFILE_PATH} --destination ${CI_REGISTRY_IMAGE}:${IMAGE_TAG} ${OPTIONAL_TAG} ${DOCKER_OPTIONS}
    -   echo "IMAGE_TAG:${IMAGE_TAG}"
    -     fi
    - fi
```

Le schéma de la pipeline :

![schema_pipeline](/images/awx-and-ara/schema_pipeline.png)

L’image générée :

![image_genere](/images/awx-and-ara/image_genere.png)

A cette étape nous avons installé, dans l’image de l'EE, le plugin de **callback** permettant de remonter les informations auprès du serveur. Il faut lui ajouter des variables d’environnements, comme ce que l’on a fait lors de **la validation du fonctionnement**.

### Saisie des variables d’environnement

AWX peut faire passer différentes variables d’environnement à ses EE. Pour la partie *callback* c’est une variable Ansible que l’on doit saisir, AWX intègre déjà cette variable dans un champ spécifique : 

`> Settings > Job Settings > Edit > **Ansible Callback Plugins**`

Ajouter le chemin du plugin du callback : `/usr/local/lib/python3.X/site-packages/ara/plugins/callback`

Pour obtenir le chemin, réalisez cette commande : `python3 -m ara.setup.callback_plugins` 

```bash
$ kubectl run get-path --image registry.gitlab.com/davidffry/ansible-builder:05236e069bed8d2679db36f286958a1c3b5ea213 -it --rm --restart Never -- python3 -m ara.setup.callback_plugins
/usr/local/lib/python3.8/site-packages/ara/plugins/callback
pod "get-path" deleted
```

La partie Callback plugin

![callback_plugin](/images/awx-and-ara/callback_plugin.png)

La partie Extra Environment Variables

![environment_var](/images/awx-and-ara/environment_var.png)

### Création d’un Environment d’exécution sous AWX

> Administration > Execution Environments > Add

![create_ee](/images/awx-and-ara/create_ee.png)

J’ai mis mon image créée en repo public le temps de la démo. 

Ajoutons l’EE au template par défaut :

![template_ee](/images/awx-and-ara/template_ee.png)

Et réalisons une exécution : 

![run_with_ee](/images/awx-and-ara/run_with_ee.png)

Voyons le retour dans Ara : 

![callback_ara](/images/awx-and-ara/callback_ara.png)

### Output AWX / Ara

#### AWX

![comparison_awx_1](/images/awx-and-ara/comparison_awx_1.png)  

![comparison_awx_2](/images/awx-and-ara/comparison_awx_2.png)  

#### Ara

![comparison_ara_1](/images/awx-and-ara/comparison_ara_1.png)  

![comparison_ara_2](/images/awx-and-ara/comparison_ara_2.png)  

## Conclusion :

En échangeant avec mes pairs, qui travaillent également avec *Ansible* et *Ansible Tower* / *Ansible Awx*, ils avaient le même problème que moi, c.a.d la difficulté de lire les logs qu'AWX nous remonte.  
Grâce à ARA nous avons trouvé une alternative pour récupérer les logs des *playbooks Ansible*
A date je n'ai pas testé la remontée des logs AWX vers un Syslog mais je doute qu'il remonte les resultats d'exécution des jobs réalisés.
Ara permet de comprendre et de stocker le résultat de l'ensemble des  traitements d'*Ansible*. Pour ma part lors de phase de *Build* d'un projet, c'est un outil indispensable à avoir. 

## Les outils utilisés :

- Clusters Managés de CIVO
- Vim
- Ara - Awx
- Gitlab : Repo - CI/CD - Container Registry
- R2devOps

## Pour aller plus loin (quelques idées) :

1. Lier Ara avec MySQL ou Postgres
2. Envoyer les logs AWX vers un centralisateur de logs
3. Backup/Restore un AWX
4. Automatiser la mise en place de ce déploiement
