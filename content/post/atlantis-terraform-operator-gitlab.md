---
title: "Utilisation d’*Atlantis* avec Gitlab pour deployer du terraform"
date: 2022-08-09T11:39:08
description: "Utilisation d’*Atlantis* avec Gitlab pour deployer du terraform"
thumbnailImagePosition: left
draft: true
thumbnailImage: https://avatars.githubusercontent.com/u/32311288?s=200&v=4
tags: 
  - "Terraform"
  - "Operator"
  - "Gitlab"
  - "Atlantis"
  - "OVH"
  - "Kubernetes"
---

![Atlantis](https://avatars.githubusercontent.com/u/32311288?s=200&v=4)

{{< toc >}}

*Atlantis* se présente comme un outil pour automatiser l’application de manifests *Terraform* via des pull requests sur les plateformes (*github/gitlab/bitbucket/AzureDevOps*) les gérants.
<!--more-->
Le but initial du projet est de faire de l’InfraAsCode en utilisant les outils de versionning et de stocker les évolutions sur chaque PR / MR. Grâce aux fonctionnalités de PR/MR dans ces outils, l’évolution est suivie et tracée.

Pourquoi j’ai voulu utiliser cet outil? Depuis plusieurs mois je cherche un outil OSS permettant de faire du *GitOps* mais pour faire de l’IaC, je suis passé par *CrossPlane* mais à date, tout n’est pas intégré pour le faire. 
En effet mon infrastructure personelle est sous OVH. Sous *Crossplane* certains objets ne sont clairement pas pris en compte, même en utilisant *[Terrajet](https://github.com/crossplane/terrajet).*

Ici nous allons gérer les noms de domaines OVH avec le provider OVH.

Avant tout nous allons mettre en place *Atlantis* avec sur l’instance “publique” de *Gitlab*

# Mise en place d’Atlantis avec le Gitlab

En suivant la documentation d’*Atlantis :* [https://www.runatlantis.io/docs/](https://www.runatlantis.io/docs/)

Voici ce qu’il faut faire côté Gitlab. Atlantis nous demande de créer un utilisateur *atlantis* mais il semblerait que je ne puisse pas le faire ou bien que je ne le sais pas le faire 😉 (hé oui, il y a des choses que je ne sais pas).

Ensuite la création du **token** (au global) est nécessaire :

![Gitlab - Preferences > Access Tokens](https://imgur.com/uCMZZMO.png)

{{< image classes="fancybox right clear" src="https://imgur.com/uCMZZMO.png" thumbnail="https://imgur.com/uCMZZMO.png" group="group:travel" thumbnail-width="150px" thumbnail-height="300px" title="A beautiful sunrise" >}}

Gitlab - Preferences > Access Tokens

Disons que mon token généré c’est `glpat-Ab2c367oPm-OrHgJpaPB` Ce token je le mets de côté. En effet il va mettre utile plus tard lors de la configuration d’*Atlantis*, dans la configuration du Chart Helm *Atlantis.*

Maintenant, nous allons générer une clé pour une communication Webhook entre Gitlab & le serveur *Atlantis.* **Qu’est-ce qu’un Webhook?** d’après [mailjet](https://www.mailjet.com/blog/email-best-practices/what-is-webhook/) : “webhooks are events that trigger an action”en français (pas super bien traduit) “les webhooks c’est pas mal”  bon je crois que j’ai mal traduit, “Les webhooks sont des évènements qui déclanchent des actions”. **En somme c’est une techno qui permet de** 

Revenons à nos moutons 🐏🐏🐏🐏🐏 :

Générons une *chaine de caractère* pour le “secret” webhook : `strings /dev/urandom | grep -o '[[:alnum:]]' | head -n 30 | base64 | tr -d '\n'; echo`

```bash
╭─david-auffray@FWWK6J3 ~ ─ (sam.,sept.10-15:11:41)()
╰─$ strings /dev/urandom | grep -o '[[:alnum:]]' | head -n 30 | base64 | tr -d '\n'; echo          
bQpoClgKRwpICm4KQwpRCmEKbQp4ClEKVgpJCkUKZQpDCjUKUQpLCnYKNQpKCngKRgpBCmgKSgo4CmkK
```

Notez qu’il faudra garder ce secret pour l’ensemble des repos que vous souhaitez gérer sur Gitlab.

## Résumons :

- nous avons récupéré 2 secrets :
    - Le Token gitlab : `glpat-Ab2c367oPm-OrHgJpaPB`
    - Le Secret du Webhook : `bQpoClgKRwpICm4KQwpRCmEKbQp4ClEKVgpJCkUKZQpDCjUKUQpLCnYKNQpKCngKRgpBCmgKSgo4CmkK`
    
    et…bah c’est tout pour le moment, nous allons passer sur la mise en place d’*Atlantis* sur mon cluster **managé** d’**OVH**
    

# Mise en place d’Atlantis

Commençons le déploiement d’*Atlantis*. Atlantis est un “simple” binaire au langage *go* qui recois les évènements *webhooks*  de votre plateforme Git pour exécuter le Terraform. Plusieurs options sont possible pour le mettre en place : 

- **Kubernetes Helm Chart**
- **Kubernetes Manifests**
- **Kubernetes Kustomize**
- **OpenShift**
- **AWS Fargate**
- **Google Kubernetes Engine (GKE)**
- **Docker**
- **Microsoft Azure**
- **Roll Your Own**

L’évoquant en préambule de ce chapitre, ainsi que dans d’autres articles (que vous trouverez ici : [https://home.dvdffry.fr/](https://home.dvdffry.fr/)) j’ai un cluster Kube managé par OVH. Je vais utiliser le *Helm Chart* fournis par Atlantis. Pour les autres je vous laisse vous en inspirer / voir la documentation 😉.

## Que nous dis la documentation :

Je vais cumuler les commandes pour l’installation du *Helm Chart*

```yaml
╭─david-auffray@FWWK6J3 ~ ─ (sam.,sept.10-15:11:41)()
╰─$ helm repo add runatlantis https://runatlantis.github.io/helm-charts
╭─david-auffray@FWWK6J3 ~ ─ (sam.,sept.10-15:11:41)()
╰─$ helm inspect values runatlantis/atlantis > values.yaml
```

Avant d’appliquer le repository, nous allons devoir créer un fichier de *values.yaml.* 

```yaml
$ helm inspect values runatlantis/atlantis > values.yaml
```

[ SPOILER ALERT ] ⇒ nous allons dans le futur de cet article, modifier ce fichier. Pour comprendre la logique, je vous conseil de ne **pas** voir le fichier *values.yaml* modifié directement et de comprendre la logique derrière.

```yaml
#values.yaml
## -------------------------- ##
# Values to override for your instance.
## -------------------------- ##

## An option to override the atlantis url,
##   if not using an ingress, set it to the external IP.
# atlantisUrl: http://10.0.0.0

# Replace this with your own repo allowlist:
-orgAllowlist: <replace-me> # Je t'ai Remplacé Mouahaha!
+orgAllowlist: gitlab.com/Davidffry/*
# logLevel: "debug"
...
# If using GitLab, specify like the following:
-# gitlab:
-#   user: foo
-#   token: bar
-#   secret: baz
# GitLab Enterprise only:
#   hostname: gitlab.your.org
# (The chart will perform the base64 encoding for you for values that are stored in secrets.)
# If using GitLab, specify like the following:
+ gitlab:
+   user: Davidffry 
+   token: glpat-Ab2c367oPm-OrHgJpaPB
+   secret: bQpoClgKRwpICm4KQwpRCmEKbQp4ClEKVgpJCkUKZQpDCjUKUQpLCnYKNQpKCngKRgpBCmgKSgo4CmkK
# GitLab Enterprise only:
#   hostname: gitlab.your.org
# (The chart will perform the base64 encoding for you for values that are stored in secrets.)
...
# Optionally specify an username and a password for basic authentication
basicAuth:
  username: "monUserPourmyConnecterAlinterfaceWebEhOuiIlestLong"
  password: "monPasswordPourMyConnecterEgalementEuh...Voila"
...
ingress:
  enabled: true
  ingressClassName:
  annotations:
    kubernetes.io/ingress.class: nginx
    cert-manager.io/cluster-issuer: letsencrypt-prod
  pathType: ImplementationSpecific
  hosts:
    - host: atlantis.exemple.fr
      paths: ["/"]
  tls:
   - secretName: atlantis-tls
     hosts:
       - atlantis.exemple.fr
  labels: {}
```

Lançons le Helm Chart …..Attendez! vous avez vu ?!

```yaml
+ gitlab:
+   user: Davidffry 
+   token: glpat-Ab2c367oPm-OrHgJpaPB
+   secret: bQpoClgKRwpICm4KQwpRCmEKbQp4ClEKVgpJCkUKZQpDCjUKUQpLCnYKNQpKCngKRgpBCmgKSgo4CmkK
```

Ce sont les valeurs que l’on a mis de côté. C’est reparti : 

```yaml
$ helm install atlantis runatlantis/atlantis -f values.yaml
```

Voilà. C’est un StatefulSet.

```bash
╭─david-auffray@FWWK6J3 ~ ─ (sam.,sept.08-20:32:32)()
╰─$ kubectl get pods --selector app=atlantis                                                 
NAME         READY   STATUS    RESTARTS   AGE
atlantis-0   1/1     Running   0          8h
```

Nous avons mis en place *Atlantis*, il attends la levés des webhooks du **Gitlab**.

# Mise en place du projet Terraform

## Le projet

## le but du projet

J’ai pour objectif de gérer de manière automatique la gestion de mes noms de domaines. D’ailleur OVH propose un provider OVHcloud qu’il a mis à jour récemment : 

{{< tweet user="aurelievache" id="1567815455473106944" >}}

## Le code terraform

Mon fichier où je gère mes variables : `my-domain.tfvars.json`

```json
{
    "records" : [{
        "zone": "exemple.fr",
        "subdomain": "rebroussepoil",
        "fieldtype": "A",
        "ttl": "0",
        "target": "51.210.253.135" #Comme vous l'aurez devinez, j'ai changé les @IP
    },
    {
        "zone": "exemple.fr",
        "subdomain": "essence",
        "fieldtype": "A",
        "ttl": "0",
        "target": "54.214.258.195" #Comme vous l'aurez devinez, j'ai changé les @IP
    }]
}
```

Ce **n’**est **pas** la bonne pratique mais je vais mettre mon code Terraform dans un seul et même fichier, histoire de pas avoir un post trop fouilli.

`terraform.tf`

```json
terraform {
  backend "http" {
  }
  required_providers {
    ovh = {
      source = "ovh/ovh"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.2.3"
    }
  }
}
variable "application_key" {
  type        = string
  sensitive   = true
  description = "the ovh application_key"
}
variable "application_secret" {
  type        = string
  sensitive   = true
  description = "the ovh application_secret"
}
variable "consumer_key" {
  type        = string
  sensitive   = true
  description = "the ovh consumer_key"
}
variable "endpoint" {
  type        = string
  sensitive   = false
  description = "the ovh endpoint"
}
variable "records" {
  type        = list(any)
  description = "Values for records"
  sensitive   = false
}
provider "ovh" {
  #application : "retrieve_domain_unlimited"
  endpoint           = var.endpoint
  application_key    = var.application_key
  application_secret = var.application_secret
  consumer_key       = var.consumer_key
}
resource "ovh_domain_zone_record" "ovh_record" {
  for_each  = { for record, v in var.records : record => v }
  zone      = each.value.zone
  subdomain = each.value.subdomain
  fieldtype = each.value.fieldtype
  ttl       = each.value.ttl
  target    = each.value.target
}
locals {
  myval = jsonencode(ovh_domain_zone_record.ovh_record)
}
```

## où stocker le fichier *.tfstate*

Une des problématiques qui s’est posé à moi lors du déploiement de mon code Terraform, c’est le lieu où j’allais stocker mon *.tfstate* du projet. Pour Altantis, il faut également pouvoir stocker, le lire et le modifier sans que cela ne se fasse à l’encontre des éléments déjà créé au préalable. La solution qui m’est apparu est de le stocker dans Gitlab. Gitlab dispose d’un endroit où l’on peu uploader et gérer son *tfstate* (en le lockant par exemple).

Lors de l’initialisation du projet il faudra spécifier où chercher ou stocker le *tfstate*. Avec Atlantis, il semblerait que l’on ne peu le faire que de 2 manières différentes :

1. Soit dans le manifest directement

```json
terraform {
  backend "http" {
		address = "https://gitlab.com/..../"
  }
...
}
```

1. Soit lors de la déclaration de l’execution. Pour ce point, l’utilisation 

## Le projet gitlab

Pushons tout cela dans un repository gitlab.

```bash
╭─david-auffray@FWWK6J3 ~/Documents/blog_atlantis ─ (sam.)()
╰─$ git init .                                                 
╭─david-auffray@FWWK6J3 ~/Documents/blog_atlantis ‹master●› ─ (sam.)()
╰─$ git add .                                                                                                                                                                                                 /0,0s
╭─david-auffray@FWWK6J3 ~/Documents/blog_atlantis ‹master●› ─ (sam.)()
╰─$ git commit -m "project: init"
[master (root-commit) 6d3b9f4] project: init
 1 file changed, 56 insertions(+)
 create mode 100644 terraform.tf                                                                                                                                                                              /0,5s
╭─david-auffray@FWWK6J3 ~/Documents/blog_atlantis ‹master› ─ (sam.)()
╰─$ git branch -m main                                                                                                                                                                                  /0,5s
╭─david-auffray@FWWK6J3 ~/Documents/blog_atlantis ‹master› ─ (sam.)()
╰─$ git remote add origin git@gitlab.com:Davidffry/blog_atlantis.git                                                                                                                                                                                    /0,5s
╭─david-auffray@FWWK6J3 ~/Documents/blog_atlantis ‹master› ─ (sam.)()
╰─$ git push -u origin main                                           
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
....
Branch 'main' set up to track remote branch 'main' from 'origin'.
╭─david-auffray@FWWK6J3 ~/Documents/blog_atlantis ‹master› ─ (sam.)()
╰─$
```

Sur l’interface **Gitlab** ajoutons le webhook, après avoir  : 

![https://imgur.com/n472ZEn.png](https://imgur.com/n472ZEn.png)

Quelles sont les options que l’on a cochés ? 

- Push events : à chaque push sur une branch une webhook sera levé
- Comment : lors de l’ajout d’un commentaire, un webhook sera également levé
- Merge Request Events : à chaque demande de fusion un webhook sera levé et traité par l’opérateur que l’on a déployé

![https://imgur.com/OylKTtE.png](https://imgur.com/OylKTtE.png)

Une fois cliqué sur *Addd webhook* le liens apparaît à la fin de la page. Nous pouvons réalisé un test à blanc pour vérifier la connectivité entre Gitlab et le endpoint Atlantis sur notre cluster cible.

Je n’ai pas mis l’output mais il va falloir me croire sur parole : **ça a fonctionné** 😉.

<aside>
🚨 Attention, dans le chapitre suivant, je ne vais vraiment pas suivre les bonnes pratiques. Je vais mettre en “dur” les valeurs de clés, secrets etc. Ce seront des valeurs d’exemple. Dans le chapitre qui suivra, je vais introduire **la gestion des secrets** mais à ma manière.

</aside>

## Déploiement du code

Etant sur OVH, pour gérer les différents objets nous devons créer un token. Pour se faire : 

![https://i.imgur.com/8KjYSxJ.png](https://i.imgur.com/8KjYSxJ.png)

Reprenons le code au préalable `terraform.tf`, juste cette partie : 

```bash
...
variable "application_key" {
  type        = string
  sensitive   = true
  description = "the ovh application_key"
	default     = "e9f6f30d706b32d6"
}
variable "application_secret" {
  type        = string
  sensitive   = true
  description = "the ovh application_secret"
	default     = "3c4b8332365a6d53452024eea8f32655"
}
variable "consumer_key" {
  type        = string
  sensitive   = true
  description = "the ovh consumer_key"
	default     = "19c914953e013f133df4412b8b358cb3"
}
variable "endpoint" {
  type        = string
  sensitive   = false
  description = "the ovh endpoint"
	default     = "ovh-eu"
}
...
```

Modifions le sur ***Gitlab*** et créons une *branch* pour réaliser un test.

![Ajout de valeurs par défaut](https://i.imgur.com/FmqqZSx.png)

Ajout de valeurs par défaut

Lors du process de MR, Gitlab envoi le Webhook vers Atlantis. Atlantis le traite et tente de lancer Terraform sur le projet en cours. Comme on peut le voir ci-dessous, l’execution se termine en erreur :

![https://i.imgur.com/Ctu7hkv.png](https://i.imgur.com/Ctu7hkv.png)

Un peu plus de détail en dessous :

![https://i.imgur.com/6iKLRjr.png](https://i.imgur.com/6iKLRjr.png)

et beaucoup plus de détail dans les logs Atlantis :

```bash
╭─david-auffray@FWWK6J3 ~ ─ (sam.)()
╰─$ k logs atlantis-0  | jq '.'
...
{
  "level": "error",
  "ts": "2022-09-12T19:40:09.263Z",
  "caller": "models/shell_command_runner.go:153",
  "msg": "running \"/usr/local/bin/terraform init -input=false -upgrade\" in \"/atlantis-data/repos/Davidffry/blog_atlantis/3/default\": exit status 1",
  "json": {
    "repo": "Davidffry/blog_atlantis",
    "pull": "3"
  },
  "stacktrace": "github.com/runatlantis/atlantis/server/core/runtime/models.(*ShellCommandRunner).RunCommandAsync.func1\n\tgithub.com/runatlantis/atlantis/server/core/runtime/models/shell_command_runner.go:153"
}
{
  "level": "error",
  "ts": "2022-09-12T19:40:09.792Z",
  "caller": "events/instrumented_project_command_runner.go:43",
  "msg": "Error running plan operation: running \"/usr/local/bin/terraform init -input=false -upgrade\" in \"/atlantis-data/repos/Davidffry/blog_atlantis/3/default\": exit status 1\n\nInitializing the backend...\n╷\n│ Error: \"address\": required field is not set\n│ \n│ \n╵\n\n",
  "json": {
    "repo": "Davidffry/blog_atlantis",
    "pull": "3"
  },
  "stacktrace": "github.com/runatlantis/atlantis/server/events.RunAndEmitStats\n\tgithub.com/runatlantis/atlantis/server/events/instrumented_project_command_runner.go:43\ngithub.com/runatlantis/atlantis/server/events.(*InstrumentedProjectCommandRunner).Plan\n\tgithub.com/runatlantis/atlantis/server/events/instrumented_project_command_runner.go:13\ngithub.com/runatlantis/atlantis/server/events.runProjectCmds\n\tgithub.com/runatlantis/atlantis/server/events/project_command_pool_executor.go:47\ngithub.com/runatlantis/atlantis/server/events.(*PlanCommandRunner).runAutoplan\n\tgithub.com/runatlantis/atlantis/server/events/plan_command_runner.go:115\ngithub.com/runatlantis/atlantis/server/events.(*PlanCommandRunner).Run\n\tgithub.com/runatlantis/atlantis/server/events/plan_command_runner.go:222\ngithub.com/runatlantis/atlantis/server/events.(*DefaultCommandRunner).RunAutoplanCommand\n\tgithub.com/runatlantis/atlantis/server/events/command_runner.go:174"
}
╭─david-auffray@FWWK6J3 ~ ─ (sam.)()
╰─$ 
```

L’erreur se trouve au niveau du *backend.*

Dans le code Terraform, j’ai demandé à ce que le *backend* soit stocké sur une plateforme en *http.* N’ayant pas indiqué où pouvais se trouver le *tfstate* en remplissant le champ “*address*” Terraform me lève une erreur.

Comme mentionné à ce niveau [où stocker le fichier *.tfstate*](https://www.notion.so/o-stocker-le-fichier-tfstate-035163bd28044f0ea6ca87a4ba5903ab), j’ai laissé la descritpion du `backend "http" {}` vide.

## La gestion des variables

## le fichier de variables

## La gestion des secrets

## Comment j’ai géré mes différentes clés

## Quels sont les autres alternatives

# Conclusion

A date, je ne sais pas si j’utiliserai *Atlantis* en production. Le fais de faire des MR pour créer mon environnement est, à mon sens,
