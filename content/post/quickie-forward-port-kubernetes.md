---
date: 2022-08-18T18:11:00
description: "Quickie - Forward de port pour les services Kubernetes"
tags: ["Quickie","Port","Network","Service","Pod","Kubernetes"]
title: "Quickie : Forward de port pour les services Kubernetes."
---
Mon Premier Quickie, nous permettant d'aller à l'essentiel sans blabla...
<!--more-->

## Quickie : Forward de port sur Kubernetes :

Avec Exemple réél : 
```bash
╭─david-auffray@FWWK6J3 ~ ─ (jeu.,août18-18:13:42)()
╰─$ k run nginx-port-forward --image=nginx:stable --restart Never --port 80 --expose
service/nginx-port-forward created
pod/nginx-port-forward created                                                                                                                                                                           /1,6s
╭─david-auffray@FWWK6J3 ~ ─ (jeu.,août18-18:14:33)()
╰─$ k port-forward --help
Forward one or more local ports to a pod.

 Use resource type/name such as deployment/mydeployment to select a pod. Resource type defaults to 'pod' if omitted.

 If there are multiple pods matching the criteria, a pod will be selected automatically. The forwarding session ends
when the selected pod terminates, and a rerun of the command is needed to resume forwarding.

Examples:
  # Listen on ports 5000 and 6000 locally, forwarding data to/from ports 5000 and 6000 in the pod
  kubectl port-forward pod/mypod 5000 6000
  
  # Listen on ports 5000 and 6000 locally, forwarding data to/from ports 5000 and 6000 in a pod selected by the
deployment
  kubectl port-forward deployment/mydeployment 5000 6000
  
  # Listen on port 8443 locally, forwarding to the targetPort of the service's port named "https" in a pod selected by
the service
  kubectl port-forward service/myservice 8443:https
  
  # Listen on port 8888 locally, forwarding to 5000 in the pod
  kubectl port-forward pod/mypod 8888:5000
  
  # Listen on port 8888 on all addresses, forwarding to 5000 in the pod
  kubectl port-forward --address 0.0.0.0 pod/mypod 8888:5000
  
  # Listen on port 8888 on localhost and selected IP, forwarding to 5000 in the pod
  kubectl port-forward --address localhost,10.19.21.23 pod/mypod 8888:5000
  
  # Listen on a random port locally, forwarding to 5000 in the pod
  kubectl port-forward pod/mypod :5000

Options:
    --address=[localhost]:
	Addresses to listen on (comma separated). Only accepts IP addresses or localhost as a value. When localhost is
	supplied, kubectl will try to bind on both 127.0.0.1 and ::1 and will fail if neither of these addresses are
	available to bind.

    --pod-running-timeout=1m0s:
	The length of time (like 5s, 2m, or 3h, higher than zero) to wait until at least one pod is running

Usage:
  kubectl port-forward TYPE/NAME [options] [LOCAL_PORT:]REMOTE_PORT [...[LOCAL_PORT_N:]REMOTE_PORT_N]

Use "kubectl options" for a list of global command-line options (applies to all commands).                                                                                                               /0,1s
╭─david-auffray@FWWK6J3 ~ ─ (jeu.,août18-18:14:53)()
╰─$ k port-forward service/nginx-port-forward 8080:80
Forwarding from 127.0.0.1:8080 -> 80
Forwarding from [::1]:8080 -> 80

#### SUR LE SECOND TERMINAL 
╭─david-auffray@FWWK6J3 ~ ─ (jeu.,août18-18:16:11)()
╰─$ curl http://localhost:8080 -v                                                     
*   Trying ::1:8080...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.68.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx/1.22.0
< Date: Thu, 18 Aug 2022 16:16:23 GMT
< Content-Type: text/html
< Content-Length: 615
< Last-Modified: Mon, 23 May 2022 23:59:19 GMT
< Connection: keep-alive
< ETag: "628c1fd7-267"
< Accept-Ranges: bytes
< 
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
* Connection #0 to host localhost left intact 
###### RETOUR SUR LE PREMIER
╭─david-auffray@FWWK6J3 ~ ─ (jeu.,août18-18:14:53)()
╰─$ k port-forward service/nginx-port-forward 8080:80
Forwarding from 127.0.0.1:8080 -> 80
Forwarding from [::1]:8080 -> 80
Handling connection for 8080
```

Source : https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/#before-you-begin
