---
title: "Ma première utilisation de CDKTF."
date: 2022-11-07T12:00:00
description: "Ma premiere utilisation de CDKTF et mon premier avis sur l'outil."
thumbnailImage: "https://avatars.githubusercontent.com/u/69365780?s=200&v=4"
thumbnailImagePosition: left
tags: 
  - "Terraform"
  - "DevOps"
  - "Hashicorp"
  - "CDKTF"
---

Lors d'un temps dédié à la veille techno, on m'a proposé de voir la partie CDKTF. Cloud development kit (nommé CDK) permet de réaliser via le langage que vous adorez la construction/paramétrage du Cloud. **Hashicorp/Terrafom** nous fourni, avec son expérience et son écosystem, un CDK pour les langages : Typescript / Go / Java / C# / Python.  
En résumé, on code en Typescript / Go / Java / C# / Python son "**Infra**" et, lors de l'exécution, CDKTF "converti" au format HCL afin que **Hashicorp/Terraform** puisse déployer ce que l'on vient de coder.

<!--more-->

![https://developer.hashicorp.com/_next/image?url=https%3A%2F%2Fcontent.hashicorp.com%2Fapi%2Fassets%3Fproduct%3Dterraform-cdk%26version%3Dv0.13.2%26asset%3Dwebsite%252Fdocs%252Fcdktf%252Fterraform-platform.png%26width%3D1776%26height%3D1317&w=1920&q=75](https://developer.hashicorp.com/_next/image?url=https%3A%2F%2Fcontent.hashicorp.com%2Fapi%2Fassets%3Fproduct%3Dterraform-cdk%26version%3Dv0.13.2%26asset%3Dwebsite%252Fdocs%252Fcdktf%252Fterraform-platform.png%26width%3D1776%26height%3D1317&w=1920&q=75)

---

{{< toc >}}

---
Disclaimer habituels : 

- C’est un projet de découverte, je ne suis pas expert de la technologie.
- C’est une découverte simple et le code n’est surement pas optimisé.
- C’est mon avis à un instant T avec mon experience. Je laisse chacun faire son propre avis.

# TL;DR

Go to the → conclusion, mais en résumé, dans un projet de Dev l’utiliser Oui, dans un projet global ou en tant qu’Ops, l’utiliser Non si nous ne sommes pas à l’aise dans un langage de programmation.

# Installation

Pour l’installation je me suis inspiré de ce que propose Terraform CDKTF : 

[Install CDK for Terraform and Run a Quick Start Demo | Terraform | HashiCorp Developer](https://developer.hashicorp.com/terraform/tutorials/cdktf/cdktf-install)

Pour aller plus vite, je vous propose une image docker avec tous les composants que j’ai installé :

Note : c’est une image qui n'est **pas** du tout optimisée, c’est du **quickstart**.

```docker
FROM ubuntu:20.04

WORKDIR /root

# update du cache d'apt
RUN apt-get update 
# Installation des packages necessaire au fonctionnement de mon **code** python
RUN apt-get install -y python3 python3-pip curl wget vim batcat
# pipenv est nécessaire pour CDKTF
RUN pip3 install pipenv
# Installation de nodejs versions 18.x
# Source : https://github.com/nodesource/distributions 
RUN curl -s https://deb.nodesource.com/setup_18.x | bash
# Installation de Terraform 
RUN wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor |  tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" |  tee /etc/apt/sources.list.d/hashicorp.list
RUN apt update &&  apt install -y terraform
# Installation du cli cdktf
RUN apt-get install -y nodejs
RUN npm install -g cdktf-cli
RUN 

CMD ["bash"]
```

Une fois l’environnement installé, j’ai commencé à définir **simplement** mon besoin (Oui, j’aurais dû le faire avant 😄) :

*Création de repos gitlab et de namespaces kubernetes de manière simple en python.*

Simple non? 

Pour ce faire j’ai utilisé 2 providers : Gitlab & Kubernetes.

**Hey mais les providers, c’est comme dans Terraform ?** Bien vu Jean-Louis, c’est tout à fais ça!

En effet on utilise les même termes que pour **Hashicorp/Terraform.**

Continuons :

```bash
cdktf init --template=python --local
```

Cette commande permet de lancer l’initation d’un projet de type *python* et en *local*. 
Lors de l’exécution, celle ci nous pose 3 questions :

- Le nom du projet
- La description du projet
- Si nous voulons remonter les rapports de **crash** aux developpeurs de CDKTF

Dans l’output, ces questions commencent par un point d’intérogation.

L’output global : 

```bash
root@terraformcdk11:~# cdktf init --template=python --local
Note: By supplying '--local' option you have chosen local storage mode for storing the state of your stack.
This means that your Terraform state file will be stored locally on disk in a file 'terraform.<STACK NAME>.tfstate' in the root of your project.
? Project Name dvdffry
? Project Description for blog demonstrating
? Do you want to send crash reports to the CDKTF team? See 
https://www.terraform.io/cdktf/create-and-deploy/configuration-file#enable-crash-reporting-for-the-cli
 for more information Yes
Warning: the environment variable LANG is not set!
We recommend setting this in ~/.profile (or equivalent) for proper expected behavior.
Creating a virtualenv for this project...
Pipfile: /root/Pipfile
Using /usr/bin/python3.8 (3.8.10) to create virtualenv...
⠹ Creating virtual environment...created virtual environment CPython3.8.10.final.0-64 in 878ms
  creator Venv(dest=/root/.local/share/virtualenvs/root-BuDEOXnJ, clear=False, no_vcs_ignore=False, global=False, describe=CPython3Posix)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/root/.local/share/virtualenv)
    added seed packages: pip==22.3, setuptools==65.5.0, wheel==0.37.1
  activators BashActivator,CShellActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator

✔ Successfully created virtual environment! 
Virtualenv location: /root/.local/share/virtualenvs/root-BuDEOXnJ
Pipfile.lock not found, creating...
Locking [packages] dependencies...
Locking [dev-packages] dependencies...
Updated Pipfile.lock (4e61a4ba9e6f02f50f68627253af5ababd9b1b4c1e10294e48158e1f42c0c5a6)!
Installing dependencies from Pipfile.lock (c0c5a6)...
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
Installing cdktf~=0.13.3...
Adding cdktf to Pipfile's [packages]...
✔ Installation Succeeded 
Pipfile.lock (c0c5a6) out of date, updating to (8a5fb0)...
Locking [packages] dependencies...
Building requirements...
Resolving dependencies...
✔ Success! 
Locking [dev-packages] dependencies...
Updated Pipfile.lock (e9caf3565c690b5fc973e6baabd2e5de4de9a5dd464768699be3bd4f418a5fb0)!
Installing dependencies from Pipfile.lock (8a5fb0)...
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
Installing pytest...
Adding pytest to Pipfile's [packages]...
✔ Installation Succeeded 
Pipfile.lock (8a5fb0) out of date, updating to (79a099)...
Locking [packages] dependencies...
Building requirements...
Resolving dependencies...
✔ Success! 
Locking [dev-packages] dependencies...
Updated Pipfile.lock (3dcedabac8d2dc7f5aec4f90d11b0d0b90f024b70dffb2aec33d7e64ba79a099)!
Installing dependencies from Pipfile.lock (79a099)...
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
========================================================================================================

  Your cdktf Python project is ready!

  cat help                Prints this message

  Compile:
    pipenv run ./main.py  Compile and run the python code.

  Synthesize:
    cdktf synth [stack]   Synthesize Terraform resources to cdktf.out/

  Diff:
    cdktf diff [stack]    Perform a diff (terraform plan) for the given stack

  Deploy:
    cdktf deploy [stack]  Deploy the given stack

  Destroy:
    cdktf destroy [stack] Destroy the given stack

  Learn more about using modules and providers https://cdk.tf/modules-and-providers

Use Providers:

  You can add prebuilt providers (if available) or locally generated ones using the add command:
  
  cdktf provider add "aws@~>3.0" null kreuzwerker/docker

  You can find all prebuilt providers on PyPI: https://pypi.org/user/cdktf-team/
  You can also install these providers directly through pipenv:

  pipenv install cdktf-cdktf-provider-aws
  pipenv install cdktf-cdktf-provider-google
  pipenv install cdktf-cdktf-provider-azurerm
  pipenv install cdktf-cdktf-provider-docker
  pipenv install cdktf-cdktf-provider-github
  pipenv install cdktf-cdktf-provider-null

  You can also build any module or provider locally. Learn more: https://cdk.tf/modules-and-providers

========================================================================================================
root@terraformcdk11:~#
```

Après le traitement, la *cli* nous génère dans le dossier un ensemble de fichiers/sous-dossiers.

```bash
root@terraformcdk11:~# ls -l
total 28
-rw-r--r-- 1 root root  152 Nov  6 14:18 Pipfile
-rw-r--r-- 1 root root 6148 Nov  6 14:18 Pipfile.lock
-rw-r--r-- 1 root root  340 Nov  6 14:18 cdktf.json
-rw-r--r-- 1 root root 1437 Nov  6 14:18 help
-rw-r--r-- 1 root root  828 Nov  6 14:18 main-test.py
-rwx------ 1 root root  294 Nov  6 14:18 main.py
```

Nous allons nous intéresser à 2 fichiers tout particulièrement :

![https://imgur.com/PxPWoTL.png](https://imgur.com/PxPWoTL.png)

## Le CDKTF.json
Le cdktf.json, nous permet de paramétrer les providers choisi ainsi que des modules etc. 
Vu que je débute dans CDKTF je n’ajoute que mes deux providers.

![https://imgur.com/7v1Dn8M.png](https://imgur.com/7v1Dn8M.png)

Il existe une autre méthode pour récupérer des providers :

```bash
  pipenv install cdktf-cdktf-provider-aws
  pipenv install cdktf-cdktf-provider-google
  pipenv install cdktf-cdktf-provider-azurerm
  pipenv install cdktf-cdktf-provider-docker
  pipenv install cdktf-cdktf-provider-github
  pipenv install cdktf-cdktf-provider-null
```

Mais je ne l’ai pas utilisé. 

Une fois les providers requis, saisie dans le fichier de configuration, il est nécessaure de les récupérer - comme pour **Hashicorp/Terraform**.

Cette ligne de commande `cdktf get` permet de télécharger les différentes *Classes* des providers en local :

```bash
root@terraformcdk11:~# cdktf get
Generated python constructs in the output directory: imports
root@terraformcdk11:~# tree
.
|-- Pipfile
|-- Pipfile.lock
|-- cdktf.json
|-- help
|-- imports
|   |-- gitlab
|   |   |-- __init__.py
|   |   |-- _jsii
|   |   |   |-- __init__.py
|   |   |   `-- gitlabhq_gitlab@0.0.0.jsii.tgz
|   |   |-- application_settings
|   |   |   `-- __init__.py
|   |   |-- branch
|   |   |   `-- __init__.py
|   |   |-- branch_protection
|   |   |   `-- __init__.py
|   |   |-- cluster_agent
|   |   |   `-- __init__.py
|   |   |-- cluster_agent_token
|   |   |   `-- __init__.py
|   |   |-- data_gitlab_branch
|   |   |   `-- __init__.py
|   |   |-- data_gitlab_cluster_agent
|   |   |   `-- __init__.py
|   |   |-- data_gitlab_cluster_agents
|   |   |   `-- __init__.py
...
|   |   |-- personal_access_token
|   |   |   `-- __init__.py
|   |   |-- pipeline_schedule
|   |   |   `-- __init__.py
|   |   |-- pipeline_schedule_variable
|   |   |   `-- __init__.py
|   |   |-- pipeline_trigger
|   |   |   `-- __init__.py
|   |   |-- project
|   |   |   `-- __init__.py
|   |   |-- project_access_token
|   |   |   `-- __init__.py
|   |   |-- project_approval_rule
|   |   |   `-- __init__.py
...
|   |   |-- system_hook
|   |   |   `-- __init__.py
|   |   |-- tag_protection
|   |   |   `-- __init__.py
|   |   |-- topic
|   |   |   `-- __init__.py
|   |   |-- user
|   |   |   `-- __init__.py
|   |   |-- user_custom_attribute
|   |   |   `-- __init__.py
|   |   |-- user_gpgkey
|   |   |   `-- __init__.py
|   |   `-- user_sshkey
|   |       `-- __init__.py
|   `-- kubernetes
|       |-- __init__.py
|       |-- _jsii
|       |   |-- __init__.py
|       |   `-- hashicorp_kubernetes@0.0.0.jsii.tgz
|       |-- annotations
|       |   `-- __init__.py
|       |-- api_service
|       |   `-- __init__.py
|       |-- api_service_v1
|       |   `-- __init__.py
|       |-- certificate_signing_request
|       |   `-- __init__.py
|       |-- certificate_signing_request_v1
|       |   `-- __init__.py
|       |-- cluster_role
|       |   `-- __init__.py
|       |-- cluster_role_binding
|       |   `-- __init__.py
|       |-- cluster_role_binding_v1
|       |   `-- __init__.py
|       |-- cluster_role_v1
|       |   `-- __init__.py
|       |-- config_map
|       |   `-- __init__.py
...
|       |-- data_kubernetes_service_v1
|       |   `-- __init__.py
|       |-- data_kubernetes_storage_class
|       |   `-- __init__.py
|       |-- data_kubernetes_storage_class_v1
|       |   `-- __init__.py
|       |-- default_service_account
|       |   `-- __init__.py
|       |-- default_service_account_v1
|       |   `-- __init__.py
|       |-- deployment
|       |   `-- __init__.py
|       |-- deployment_v1
|       |   `-- __init__.py
|       |-- endpoints
|       |   `-- __init__.py
|       |-- endpoints_v1
|       |   `-- __init__.py
...
|       |-- namespace
|       |   `-- __init__.py
|       |-- namespace_v1
|       |   `-- __init__.py
|       |-- network_policy
|       |   `-- __init__.py
|       |-- network_policy_v1
|       |   `-- __init__.py
|       |-- persistent_volume
|       |   `-- __init__.py
|       |-- persistent_volume_claim
|       |   `-- __init__.py
|       |-- persistent_volume_claim_v1
|       |   `-- __init__.py
|       |-- persistent_volume_v1
|       |   `-- __init__.py
|       |-- pod
|       |   `-- __init__.py
...
|       |-- storage_class
|       |   `-- __init__.py
|       |-- storage_class_v1
|       |   `-- __init__.py
|       |-- validating_webhook_configuration
|       |   `-- __init__.py
|       `-- validating_webhook_configuration_v1
|           `-- __init__.py
|-- main-test.py
|-- main.py
`-- tree.file

204 directories, 214 files
```

Voilà, nous sommes prêts à coder = > Go sur le fichier [main.py](http://main.py) :

## Le main.py

```python
#!/usr/bin/env python
import os
from constructs import Construct
from cdktf import App, TerraformStack

from imports.kubernetes.provider import KubernetesProvider # Add by me
from imports.kubernetes.namespace import Namespace # Add by me
from imports.gitlab.provider import GitlabProvider # Add by me
from imports.gitlab.project import Project # Add by me

ldef = ['dev','int','ppd','prd'] # Add by me
def mylab (oldterms) : # Add by me
    return oldterms+"-cdktf"

mytoken = os.getenv('TOKEN') # Add by me

class MyStack(TerraformStack):
    def __init__(self, scope: Construct, id: str):
        super().__init__(scope, id)

        # define resources here

        KubernetesProvider(self, 'kind', config_path="~/.kubeconfig") # Add by me

        GitlabProvider(self,'myglprovider',base_url="https://gitlab.com",token=mytoken) # Add by me

        for env in ldef: # Add by me
            Namespace(self,"namespace"+env,metadata = { # Add by me
                'name': "monptoj-"+env # Add by me
            }) # Add by me
            Project(self,"myproj"+env,name=mylab("monptoj-"+env)) # Add by me

app = App()
MyStack(app, "root")

app.synth()
```

Ici, il vous manquera deux éléments pour exécuter ce code : 

- Votre kubeconfig
- Votre Personnal Access Token de GITLAB

Je vous laisse les mettre en place. 😜

**Mais comment connais-tu les *Classes* à mettre en place/ utiliser ?!** C’est assez simple Jean-Louis, un repository existe où est répertorié l ensemble des providers disponible. Le lien : [https://github.com/orgs/cdktf/repositories](https://github.com/orgs/cdktf/repositories)

Exemple pour le provider Azure : 

[https://github.com/cdktf/cdktf-provider-azurerm](https://github.com/cdktf/cdktf-provider-azurerm)

Ainsi que la documentation avec les *Classes* associées :

[https://constructs.dev/packages/@cdktf/provider-azurerm/v/3.0.15/api/ApiConnection?lang=python&submodule=apiConnection](https://constructs.dev/packages/@cdktf/provider-azurerm/v/3.0.15/api/ApiConnection?lang=python&submodule=apiConnection)

![https://imgur.com/xuRBdX4.png](https://imgur.com/xuRBdX4.png)

# Run

Avant l’exécution du code nous avions : 

![https://imgur.com/ohzWaNe.png](https://imgur.com/ohzWaNe.png)

Après exécution du code CDKTF : 

![https://imgur.com/zDVfmBR.png](https://imgur.com/zDVfmBR.png)

Pour créer on utilise la commande `cdktf deploy` et `cdktf destroy`pour la supprimer.

On remarque également qu’il réalise un “plan” avant de l’executer.

Côté Output de la commande, celle-ci ressemble beaucoup à ce que propose Hashicorp/Terraform :

```
root@terraformcdk10:~# date
Mon Nov  7 11:19:12 UTC 2022
root@terraformcdk10:~# cdktf deploy
root  Initializing the backend...
root  Initializing provider plugins...
      - Reusing previous version of gitlabhq/gitlab from the dependency lock file
root  - Reusing previous version of hashicorp/kubernetes from the dependency lock file
root  - Using previously-installed gitlabhq/gitlab v3.18.0
root  - Using previously-installed hashicorp/kubernetes v2.15.0

      Terraform has been successfully initialized!
root  
      You may now begin working with Terraform. Try running "terraform plan" to see
      any changes that are required for your infrastructure. All Terraform commands
      should now work.

      If you ever set or change modules or backend configuration for Terraform,
      rerun this command to reinitialize your working directory. If you forget, other
      commands will detect it and remind you to do so if necessary.
root  Terraform used the selected providers to generate the following execution
      plan. Resource actions are indicated with the following symbols:
        + create
      
      Terraform will perform the following actions:
root    # gitlab_project.myprojdev (myprojdev) will be created
        + resource "gitlab_project" "myprojdev" {
            + allow_merge_on_skipped_pipeline                  = false
            + analytics_access_level                           = (known after apply)
            + approvals_before_merge                           = 0
            + archived                                         = false
            + auto_cancel_pending_pipelines                    = (known after apply)
            + auto_devops_deploy_strategy                      = (known after apply)
            + auto_devops_enabled                              = (known after apply)
            + autoclose_referenced_issues                      = (known after apply)
            + build_git_strategy                               = (known after apply)
            + build_timeout                                    = (known after apply)
            + builds_access_level                              = (known after apply)
            + ci_default_git_depth                             = (known after apply)
            + ci_forward_deployment_enabled                    = true
            + container_registry_access_level                  = (known after apply)
            + container_registry_enabled                       = true
            + default_branch                                   = (known after apply)
            + forking_access_level                             = (known after apply)
            + http_url_to_repo                                 = (known after apply)
            + id                                               = (known after apply)
            + issues_access_level                              = (known after apply)
            + issues_enabled                                   = true
            + lfs_enabled                                      = true
            + merge_method                                     = "merge"
            + merge_pipelines_enabled                          = false
            + merge_requests_access_level                      = (known after apply)
            + merge_requests_enabled                           = true
            + merge_trains_enabled                             = false
            + mirror                                           = false
            + mirror_overwrites_diverged_branches              = false
            + mirror_trigger_builds                            = false
            + name                                             = "monptoj-dev-cdktf"
            + namespace_id                                     = (known after apply)
            + only_allow_merge_if_all_discussions_are_resolved = false
            + only_allow_merge_if_pipeline_succeeds            = false
            + only_mirror_protected_branches                   = false
            + operations_access_level                          = (known after apply)
            + packages_enabled                                 = true
            + pages_access_level                               = "private"
            + path_with_namespace                              = (known after apply)
            + pipelines_enabled                                = true
            + printing_merge_request_link_enabled              = true
            + repository_access_level                          = (known after apply)
            + repository_storage                               = (known after apply)
            + request_access_enabled                           = true
            + requirements_access_level                        = (known after apply)
            + runners_token                                    = (sensitive value)
            + security_and_compliance_access_level             = (known after apply)
            + shared_runners_enabled                           = (known after apply)
            + snippets_access_level                            = (known after apply)
            + snippets_enabled                                 = true
            + squash_option                                    = "default_off"
            + ssh_url_to_repo                                  = (known after apply)
            + tags                                             = (known after apply)
            + topics                                           = (known after apply)
            + visibility_level                                 = "private"
            + web_url                                          = (known after apply)
            + wiki_access_level                                = (known after apply)
            + wiki_enabled                                     = true

            + container_expiration_policy {
                + cadence           = (known after apply)
                + enabled           = (known after apply)
                + keep_n            = (known after apply)
                + name_regex_delete = (known after apply)
                + name_regex_keep   = (known after apply)
                + next_run_at       = (known after apply)
                + older_than        = (known after apply)
              }

            + push_rules {
                + author_email_regex            = (known after apply)
                + branch_name_regex             = (known after apply)
                + commit_committer_check        = (known after apply)
                + commit_message_negative_regex = (known after apply)
                + commit_message_regex          = (known after apply)
                + deny_delete_tag               = (known after apply)
                + file_name_regex               = (known after apply)
                + max_file_size                 = (known after apply)
                + member_check                  = (known after apply)
                + prevent_secrets               = (known after apply)
                + reject_unsigned_commits       = (known after apply)
              }
          }
root    # gitlab_project.myprojint (myprojint) will be created
        + resource "gitlab_project" "myprojint" {
            + allow_merge_on_skipped_pipeline                  = false
            + analytics_access_level                           = (known after apply)
            + approvals_before_merge                           = 0
            + archived                                         = false
            + auto_cancel_pending_pipelines                    = (known after apply)
            + auto_devops_deploy_strategy                      = (known after apply)
            + auto_devops_enabled                              = (known after apply)
            + autoclose_referenced_issues                      = (known after apply)
            + build_git_strategy                               = (known after apply)
            + build_timeout                                    = (known after apply)
            + builds_access_level                              = (known after apply)
            + ci_default_git_depth                             = (known after apply)
            + ci_forward_deployment_enabled                    = true
            + container_registry_access_level                  = (known after apply)
            + container_registry_enabled                       = true
            + default_branch                                   = (known after apply)
            + forking_access_level                             = (known after apply)
            + http_url_to_repo                                 = (known after apply)
            + id                                               = (known after apply)
            + issues_access_level                              = (known after apply)
            + issues_enabled                                   = true
            + lfs_enabled                                      = true
            + merge_method                                     = "merge"
            + merge_pipelines_enabled                          = false
            + merge_requests_access_level                      = (known after apply)
            + merge_requests_enabled                           = true
            + merge_trains_enabled                             = false
            + mirror                                           = false
            + mirror_overwrites_diverged_branches              = false
            + mirror_trigger_builds                            = false
            + name                                             = "monptoj-int-cdktf"
            + namespace_id                                     = (known after apply)
            + only_allow_merge_if_all_discussions_are_resolved = false
            + only_allow_merge_if_pipeline_succeeds            = false
            + only_mirror_protected_branches                   = false
            + operations_access_level                          = (known after apply)
            + packages_enabled                                 = true
            + pages_access_level                               = "private"
            + path_with_namespace                              = (known after apply)
            + pipelines_enabled                                = true
            + printing_merge_request_link_enabled              = true
            + repository_access_level                          = (known after apply)
            + repository_storage                               = (known after apply)
            + request_access_enabled                           = true
            + requirements_access_level                        = (known after apply)
            + runners_token                                    = (sensitive value)
            + security_and_compliance_access_level             = (known after apply)
            + shared_runners_enabled                           = (known after apply)
            + snippets_access_level                            = (known after apply)
            + snippets_enabled                                 = true
            + squash_option                                    = "default_off"
            + ssh_url_to_repo                                  = (known after apply)
            + tags                                             = (known after apply)
            + topics                                           = (known after apply)
            + visibility_level                                 = "private"
            + web_url                                          = (known after apply)
            + wiki_access_level                                = (known after apply)
            + wiki_enabled                                     = true

            + container_expiration_policy {
                + cadence           = (known after apply)
                + enabled           = (known after apply)
                + keep_n            = (known after apply)
                + name_regex_delete = (known after apply)
                + name_regex_keep   = (known after apply)
                + next_run_at       = (known after apply)
                + older_than        = (known after apply)
              }

            + push_rules {
                + author_email_regex            = (known after apply)
                + branch_name_regex             = (known after apply)
                + commit_committer_check        = (known after apply)
                + commit_message_negative_regex = (known after apply)
                + commit_message_regex          = (known after apply)
                + deny_delete_tag               = (known after apply)
                + file_name_regex               = (known after apply)
                + max_file_size                 = (known after apply)
                + member_check                  = (known after apply)
                + prevent_secrets               = (known after apply)
                + reject_unsigned_commits       = (known after apply)
              }
          }

        # gitlab_project.myprojppd (myprojppd) will be created
        + resource "gitlab_project" "myprojppd" {
            + allow_merge_on_skipped_pipeline                  = false
            + analytics_access_level                           = (known after apply)
            + approvals_before_merge                           = 0
            + archived                                         = false
            + auto_cancel_pending_pipelines                    = (known after apply)
            + auto_devops_deploy_strategy                      = (known after apply)
            + auto_devops_enabled                              = (known after apply)
            + autoclose_referenced_issues                      = (known after apply)
            + build_git_strategy                               = (known after apply)
            + build_timeout                                    = (known after apply)
            + builds_access_level                              = (known after apply)
            + ci_default_git_depth                             = (known after apply)
            + ci_forward_deployment_enabled                    = true
            + container_registry_access_level                  = (known after apply)
            + container_registry_enabled                       = true
            + default_branch                                   = (known after apply)
            + forking_access_level                             = (known after apply)
            + http_url_to_repo                                 = (known after apply)
            + id                                               = (known after apply)
            + issues_access_level                              = (known after apply)
            + issues_enabled                                   = true
            + lfs_enabled                                      = true
            + merge_method                                     = "merge"
            + merge_pipelines_enabled                          = false
            + merge_requests_access_level                      = (known after apply)
            + merge_requests_enabled                           = true
            + merge_trains_enabled                             = false
            + mirror                                           = false
            + mirror_overwrites_diverged_branches              = false
            + mirror_trigger_builds                            = false
            + name                                             = "monptoj-ppd-cdktf"
            + namespace_id                                     = (known after apply)
            + only_allow_merge_if_all_discussions_are_resolved = false
            + only_allow_merge_if_pipeline_succeeds            = false
            + only_mirror_protected_branches                   = false
            + operations_access_level                          = (known after apply)
            + packages_enabled                                 = true
            + pages_access_level                               = "private"
            + path_with_namespace                              = (known after apply)
            + pipelines_enabled                                = true
            + printing_merge_request_link_enabled              = true
            + repository_access_level                          = (known after apply)
            + repository_storage                               = (known after apply)
            + request_access_enabled                           = true
            + requirements_access_level                        = (known after apply)
            + runners_token                                    = (sensitive value)
            + security_and_compliance_access_level             = (known after apply)
            + shared_runners_enabled                           = (known after apply)
            + snippets_access_level                            = (known after apply)
            + snippets_enabled                                 = true
            + squash_option                                    = "default_off"
            + ssh_url_to_repo                                  = (known after apply)
            + tags                                             = (known after apply)
            + topics                                           = (known after apply)
            + visibility_level                                 = "private"
            + web_url                                          = (known after apply)
            + wiki_access_level                                = (known after apply)
            + wiki_enabled                                     = true

            + container_expiration_policy {
                + cadence           = (known after apply)
                + enabled           = (known after apply)
                + keep_n            = (known after apply)
                + name_regex_delete = (known after apply)
                + name_regex_keep   = (known after apply)
                + next_run_at       = (known after apply)
                + older_than        = (known after apply)
              }

            + push_rules {
                + author_email_regex            = (known after apply)
                + branch_name_regex             = (known after apply)
                + commit_committer_check        = (known after apply)
                + commit_message_negative_regex = (known after apply)
                + commit_message_regex          = (known after apply)
                + deny_delete_tag               = (known after apply)
                + file_name_regex               = (known after apply)
                + max_file_size                 = (known after apply)
                + member_check                  = (known after apply)
                + prevent_secrets               = (known after apply)
                + reject_unsigned_commits       = (known after apply)
              }
          }

        # gitlab_project.myprojprd (myprojprd) will be created
        + resource "gitlab_project" "myprojprd" {
            + allow_merge_on_skipped_pipeline                  = false
            + analytics_access_level                           = (known after apply)
            + approvals_before_merge                           = 0
            + archived                                         = false
            + auto_cancel_pending_pipelines                    = (known after apply)
            + auto_devops_deploy_strategy                      = (known after apply)
            + auto_devops_enabled                              = (known after apply)
            + autoclose_referenced_issues                      = (known after apply)
            + build_git_strategy                               = (known after apply)
            + build_timeout                                    = (known after apply)
            + builds_access_level                              = (known after apply)
            + ci_default_git_depth                             = (known after apply)
            + ci_forward_deployment_enabled                    = true
            + container_registry_access_level                  = (known after apply)
            + container_registry_enabled                       = true
            + default_branch                                   = (known after apply)
            + forking_access_level                             = (known after apply)
            + http_url_to_repo                                 = (known after apply)
            + id                                               = (known after apply)
            + issues_access_level                              = (known after apply)
            + issues_enabled                                   = true
            + lfs_enabled                                      = true
            + merge_method                                     = "merge"
            + merge_pipelines_enabled                          = false
            + merge_requests_access_level                      = (known after apply)
            + merge_requests_enabled                           = true
            + merge_trains_enabled                             = false
            + mirror                                           = false
            + mirror_overwrites_diverged_branches              = false
            + mirror_trigger_builds                            = false
            + name                                             = "monptoj-prd-cdktf"
            + namespace_id                                     = (known after apply)
            + only_allow_merge_if_all_discussions_are_resolved = false
            + only_allow_merge_if_pipeline_succeeds            = false
            + only_mirror_protected_branches                   = false
            + operations_access_level                          = (known after apply)
            + packages_enabled                                 = true
            + pages_access_level                               = "private"
            + path_with_namespace                              = (known after apply)
            + pipelines_enabled                                = true
            + printing_merge_request_link_enabled              = true
            + repository_access_level                          = (known after apply)
            + repository_storage                               = (known after apply)
            + request_access_enabled                           = true
            + requirements_access_level                        = (known after apply)
            + runners_token                                    = (sensitive value)
            + security_and_compliance_access_level             = (known after apply)
            + shared_runners_enabled                           = (known after apply)
            + snippets_access_level                            = (known after apply)
            + snippets_enabled                                 = true
            + squash_option                                    = "default_off"
            + ssh_url_to_repo                                  = (known after apply)
            + tags                                             = (known after apply)
            + topics                                           = (known after apply)
            + visibility_level                                 = "private"
            + web_url                                          = (known after apply)
            + wiki_access_level                                = (known after apply)
            + wiki_enabled                                     = true

            + container_expiration_policy {
                + cadence           = (known after apply)
                + enabled           = (known after apply)
                + keep_n            = (known after apply)
                + name_regex_delete = (known after apply)
                + name_regex_keep   = (known after apply)
                + next_run_at       = (known after apply)
                + older_than        = (known after apply)
              }

            + push_rules {
                + author_email_regex            = (known after apply)
                + branch_name_regex             = (known after apply)
                + commit_committer_check        = (known after apply)
                + commit_message_negative_regex = (known after apply)
                + commit_message_regex          = (known after apply)
                + deny_delete_tag               = (known after apply)
                + file_name_regex               = (known after apply)
                + max_file_size                 = (known after apply)
                + member_check                  = (known after apply)
                + prevent_secrets               = (known after apply)
                + reject_unsigned_commits       = (known after apply)
              }
          }

        # kubernetes_namespace.namespacedev (namespacedev) will be created
        + resource "kubernetes_namespace" "namespacedev" {
            + id = (known after apply)

            + metadata {
                + generation       = (known after apply)
                + name             = "monptoj-dev"
                + resource_version = (known after apply)
                + uid              = (known after apply)
              }
          }

        # kubernetes_namespace.namespaceint (namespaceint) will be created
        + resource "kubernetes_namespace" "namespaceint" {
            + id = (known after apply)

            + metadata {
                + generation       = (known after apply)
                + name             = "monptoj-int"
                + resource_version = (known after apply)
                + uid              = (known after apply)
              }
          }

        # kubernetes_namespace.namespaceppd (namespaceppd) will be created
        + resource "kubernetes_namespace" "namespaceppd" {
            + id = (known after apply)

            + metadata {
                + generation       = (known after apply)
                + name             = "monptoj-ppd"
                + resource_version = (known after apply)
                + uid              = (known after apply)
              }
          }

        # kubernetes_namespace.namespaceprd (namespaceprd) will be created
        + resource "kubernetes_namespace" "namespaceprd" {
            + id = (known after apply)

            + metadata {
                + generation       = (known after apply)
                + name             = "monptoj-prd"
                + resource_version = (known after apply)
                + uid              = (known after apply)
              }
          }

      Plan: 8 to add, 0 to change, 0 to destroy.
      
      ─────────────────────────────────────────────────────────────────────────────

      Saved the plan to: plan

      To perform exactly these actions, run the following command to apply:
          terraform apply "plan"

Please review the diff output above for root
❯ Approve  Applies the changes outlined in the plan.
  Dismiss
  Stop

      Saved the plan to: plan

      To perform exactly these actions, run the following command to apply:
          terraform apply "plan"
root  kubernetes_namespace.namespacedev (namespacedev): Creating...
      kubernetes_namespace.namespaceint (namespaceint): Creating...
      kubernetes_namespace.namespaceppd (namespaceppd): Creating...
root  kubernetes_namespace.namespaceprd (namespaceprd): Creating...
root  kubernetes_namespace.namespaceint (namespaceint): Creation complete after 0s [id=monptoj-int]
root  kubernetes_namespace.namespaceprd (namespaceprd): Creation complete after 1s [id=monptoj-prd]
root  kubernetes_namespace.namespacedev (namespacedev): Creation complete after 1s [id=monptoj-dev]
root  kubernetes_namespace.namespaceppd (namespaceppd): Creation complete after 1s [id=monptoj-ppd]
root  gitlab_project.myprojdev (myprojdev): Creating...
      gitlab_project.myprojprd (myprojprd): Creating...
      gitlab_project.myprojint (myprojint): Creating...
root  gitlab_project.myprojppd (myprojppd): Creating...
root  gitlab_project.myprojdev (myprojdev): Creation complete after 3s [id=40835843]
root  gitlab_project.myprojppd (myprojppd): Creation complete after 4s [id=40835844]
root  gitlab_project.myprojprd (myprojprd): Creation complete after 4s [id=40835842]
root  gitlab_project.myprojint (myprojint): Creation complete after 6s [id=40835847]
root  
      Apply complete! Resources: 8 added, 0 changed, 0 destroyed.
      

No outputs found.
```

# Les uses case CDKTF

En échangeant avec mes pairs, j’ai pu identifié plusieurs cas d’utilisation de CDKTF, même si nous n’étions pas d’accord sur certains points.

- Rendre la vie des développeu.r.se.s de *****[Mettez le langage de prog (python, go, c#, java, typescript) que vous souhaitez]***** plus simple en les rendants moins dépendant d’autres équipes.
- Garder les développeu.r.se.s dans leur langage de prédilections. Ils n’auront pas besoin d’apprendre le langage **Hashicorp/Terraform** qui a quelques subtilités (boucles notamment).
- Permettre de créer des apis personnalisées, utilisation de flask par exemple en python.
- Créer des **fonctions** custom, indépendant du code d’infra **Hashicorp/Terraform**. Cela peut permettre d’étendre les champs de possiblité avec le code **Hashicorp/Terraform**.

Dans la documentation de CDKTF, Hashicorp met également, que l'on ne peut pas toujours l'utiliser en lieu et place d'**Hashicorp/Terraform**.
voici les uses cases présenté dans la documentation :


# Conclusion 

En ce qui me concerne, je pense que le projet CDKTF, est un projet qui va évoluer proposant plus de fonctionnalité/langage. Il existe une conversion HCL → langage de prog mais pour ce que j’ai pu essayé je trouve qu’il n’est pas “sec”.

En tant qu’Ops, est ce que je partirai sur du CDKTF? Non, je ne pense pas partir sur ce genre d’implémentation côté code. En effet je ne connais pas toutes les subtiltés des langages de programmation. Si je dois partager avec mes collègues Ops (dans le cas où ils font également du TF) ce sera plus facile à maintenir, à faire évoluer etc.

Et en tant que Dev ? Oui, puisque cela me permet d’intégrer du **Hashicorp/Terraform** directement dans mon code pour mes projets. En tant que dev, cela me permet d’utiliser et de créer de l’IAAS dans le langage que je maîtrise sans devoir apprendre le HCL.

Notons quand même, que l’utilisation de CDKTF permet d’étendre “plus facilement” les fonctions native de **Hashicorp/Terraform**, et que nous pouvons utiliser les modules **Hashicorp/Terraform** déjà créé, sous réserve que les providers existe sous CDKTF.

# Sources 
- [https://youtu.be/bssG1piyaKw](https://youtu.be/bssG1piyaKw)  
- [https://github.com/hashicorp/terraform-cdk/tree/main/examples/python](https://github.com/hashicorp/terraform-cdk/tree/main/examples/python)  
- [https://developer.hashicorp.com/terraform/cdktf](https://developer.hashicorp.com/terraform/cdktf)  
