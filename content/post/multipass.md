---
date: 2022-06-09T10:58:08-04:00
description: "Multipass Generateur de VMs en ligne de commande."
featured_image: "/images/Pope-Edouard-de-Beaumont-1844.jpg"
tags: ["Multipass","server","VM"]
title: "Multipass"
thumbnailImagePosition: left
thumbnailImage: https://assets.ubuntu.com/v1/0698ab2d-muiltipass-promo-header.png
---
Multipass est un outil permettant d’avoir à disposition une machine virtuelle en utilisant une CLI. Cette CLI permet d’exécuter, de gérer et de manipuler des instances (vms) Linux. Cet outil télécharge une image récente en quelques instant (en fonction de votre débit internet, ou si l’image est déjà présente sur la machine). C’est du : **Up & Running**.

<!--more-->
L’outil est très similaire à Vagrant mais se veut plus simple et plus rapide. Si nous souhaitons une image de base (ubuntu) une seule ligne de commande : paf `multipass launch --name quick_instance`

## Installation

Sur linux (ubuntu) : $`sudo snap install multipass`

**Sur Windows :** 

Télécharger : [https://multipass.run/download/windows](https://multipass.run/download/windows)

Executer (avec les privilèges administrateurs) et installez le.

## Exploitation

Exploitation simple : 

```bash
$ multipass --help                                                                                                                                                                255 ↵
Usage: multipass [options] <command>
Create, control and connect to Ubuntu instances.

This is a command line utility for multipass, a
service that manages Ubuntu instances.

Options:
  -h, --help     Display this help
  -v, --verbose  Increase logging verbosity. Repeat the 'v' in the short option
                 for more detail. Maximum verbosity is obtained with 4 (or more)
                 v, i.e. -vvvv.

Available commands:
  alias         Create an alias
  aliases       List available aliases
  authenticate  Authenticate client
  delete        Delete instances
  exec          Run a command on an instance
  find          Display available images to create instances from
  get           Get a configuration setting
  help          Display help about a command
  info          Display information about instances
  launch        Create and start an Ubuntu instance
  list          List all available instances
  mount         Mount a local directory in the instance
  networks      List available network interfaces
  purge         Purge all deleted instances permanently
  recover       Recover deleted instances
  restart       Restart instances
  set           Set a configuration setting
  shell         Open a shell on a running instance
  start         Start instances
  stop          Stop running instances
  suspend       Suspend running instances
  transfer      Transfer files between the host and instances
  umount        Unmount a directory from an instance
  unalias       Remove an alias
  version       Show version details

$ multipass launch --name dvdauay
Launched: dvdauay

$ multipass list
Name                    State             IPv4             Image
primary                 Deleted           --               Not Available
dvdauay                 Running           10.237.53.228    Ubuntu 20.04 LTS
foo                     Deleted           --               Not Available

$ multipass purge

$ multipass list 
Name                    State             IPv4             Image
dvdauay                 Running           10.237.53.228    Ubuntu 20.04 LTS

$ multipass info dvdauay                                                                                                                                                          130 ↵
Name:           dvdauay
State:          Running
IPv4:           10.237.53.228
Release:        Ubuntu 20.04.4 LTS
Image hash:     9a0f10025864 (Ubuntu 20.04 LTS)
Load:           0.08 0.07 0.06
Disk usage:     1.3G out of 4.7G
Memory usage:   145.6M out of 976.8M
Mounts:         --

$ multipass info --help                                                                                                                                                             1 ↵
Usage: multipass info [options] <name> [<name> ...]
Display information about instances

Options:
  -h, --help         Display this help
  -v, --verbose      Increase logging verbosity. Repeat the 'v' in the short
                     option for more detail. Maximum verbosity is obtained with
                     4 (or more) v, i.e. -vvvv.
  --all              Display info for all instances
  --format <format>  Output info in the requested format.
                     Valid formats are: table (default), json, csv and yaml

Arguments:
  name               Names of instances to display information about

$ multipass info --all --format yaml
errors:
  - ~
dvdauay:
  - state: Running
    image_hash: 9a0f100258640fa68ee13e33f4ea636ee3f82e2b9991381553be65121ce3657b
    image_release: 20.04 LTS
    release: Ubuntu 20.04.4 LTS
    load:
      - 0.01
      - 0.04
      - 0.05
    disks:
      - sda1:
          used: 1446449152
          total: 5019643904
    memory:
      usage: 152260608
      total: 1024241664
    ipv4:
      - 10.237.53.228
    mounts: ~

$ multipass shell dvdauay
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.4.0-109-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Thu May 19 08:50:34 CEST 2022

  System load:  0.05              Processes:             103
  Usage of /:   28.8% of 4.67GB   Users logged in:       0
  Memory usage: 20%               IPv4 address for ens3: 10.237.53.228
  Swap usage:   0%

 * Super-optimized for small spaces - read how we shrank the memory
   footprint of MicroK8s to make it the smallest full K8s around.

   https://ubuntu.com/blog/microk8s-memory-optimisation

1 update can be applied immediately.
To see these additional updates run: apt list --upgradable

The list of available updates is more than a week old.
To check for new updates run: sudo apt update

To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

ubuntu@dvdauay:~$
```

Pour aller un peu plus loin, voici l’execution de multipass vue du système : 

```bash
nobody      2471  0.0  0.0   9244  3132 ?        S    mai18   0:00 /snap/multipass/6920/usr/sbin/dnsmasq --keep-in-foreground --strict-order --bind-interfaces --pid-file --domain=multipass --local=/multipass/ --except-interface=lo --interface=mpqemubr0 --listen-address=10.237.53.1 --dhcp-no-override --dhcp-authoritative --dhcp-leasefile=/var/snap/multipass/common/data/multipassd/network/dnsmasq.leases --dhcp-hostsfile=/var/snap/multipass/common/data/multipassd/network/dnsmasq.hosts --dhcp-range 10.237.53.2,10.237.53.254,infinite --conf-file=/var/snap/multipass/common/data/multipassd/network/dnsmasq-nUFNOp.conf
root      603418  1.9  4.9 2762352 783416 ?      Sl   08:33   0:27 /snap/multipass/6920/usr/bin/qemu-system-x86_64 --enable-kvm -cpu host -nic tap,ifname=tap-bbafb8742fd,script=no,downscript=no,model=virtio-net-pci,mac=52:54:00:27:d5:8c -device virtio-scsi-pci,id=scsi0 -drive file=/var/snap/multipass/common/data/multipassd/vault/instances/dvdauay/ubuntu-20.04-server-cloudimg-amd64.img,if=none,format=qcow2,discard=unmap,id=hda -device scsi-hd,drive=hda,bus=scsi0.0 -smp 1 -m 1024M -qmp stdio -chardev null,id=char0 -serial chardev:char0 -nographic -cdrom /var/snap/multipass/common/data/multipassd/vault/instances/dvdauay/cloud-init-config.iso
```

Bon David, c’est bien d’avoir une machine virtuelle clean mais…..pour les tests ce n’est pas suffisant.

## Cloud-Init File

Cloud-init est une méthode permettant de standardiser l'initialisation d'instances cloud sur de multiple plateformes. Il est pris en charge par tous les principaux fournisseurs de cloud public, les systèmes de provisionnement pour l'infrastructure de cloud privé et les installations sans système d'exploitation.

Le fichier *yaml* peut être utilisé pour la distribution Ubuntu, mais pas seulement. Pour d’autres distribution ( ex : RHEL, Alpine Linux, Debian) pour initaliser différentes instances sur les clouds provider (ex : AWS, Azure, Scaleway) et également des clouds “On-premise“ (VMware, LXD, KVM).

Il est utilisable également dans le déploiement d’image *Multipass*.

[Availability - cloud-init 21.4 documentation](https://cloudinit.readthedocs.io/en/latest/topics/availability.html#distributions)

un exemple de *cloud-init file* permettant d'ajouter une clé publique à l'image que l'on va utiliser dans le multipass :

```yaml
# cloud-config.yaml
---
users:
  - name: ubuntu
    gecos: ubuntu user
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: users, admin
    lock_passwd: true
    ssh_authorized_keys:
      - <PUBLIC KEY>
```

```powershell
multipass launch --name dvdauay --cloud-init cloud-config.yaml
multipass launch --name other_dvdauay --cloud-init cloud-config.yaml --network XYZ
```

Ici mon image ubuntu par défaut est initialisé avec le fichier *cloud-config.yaml.* Ce fichier *.yaml* à pour but d’ajouter à mon image, l’utilisateur *ubuntu* et la clé publique associée. Cela me permet de m’y connecter sans utiliser de mots de passe et en utilisant ma clé privée.

D’autres configurations cloud init sont donc possible. En voici d’autres (elles sont séparées par les `---`) : 

```yaml
---
manage_resolv_conf: true

resolv_conf:
  nameservers: ['8.8.4.4', '8.8.8.8']
  searchdomains:
    - foo.example.com
  domain: example.com
  options:
    rotate: true
    timeout: 1
---
apt:
  primary:
    - arches: [default]
      uri: http://us.archive.ubuntu.com/ubuntu/
# or
apt:
  primary:
    - arches: [default]
      search:
        - http://local-mirror.mydomain
        - http://archive.ubuntu.com
---
final_message: "The system is finally up, after $UPTIME seconds"
---
# installation de manière agnostique de différents pacakges
packages:
 - pwgen
 - pastebinit
 - [libpython2.7, 2.7.3-0ubuntu3.1]
---
packages_update: true
---
# add each entry to ~/.ssh/authorized_keys for the configured user or the
# first user defined in the user definition directive.
ssh_authorized_keys:
  - ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAGEA3FSyQwBI6Z+nCSjUUk8EEAnnkhXlukKoUPND/RRClWz2s5TCzIkd3Ou5+Cyz71X0XmazM3l5WgeErvtIwQMyT1KjNoMhoJMrJnWqQPOt5Q8zWd9qG7PBl9+eiH5qV7NZ mykey@host
  - ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA3I7VUf2l5gSn5uavROsc5HRDpZdQueUq5ozemNSj8T7enqKHOEaFoU2VoPgGEWC9RyzSQVeyD6s7APMcE82EtmW4skVEgEGSbDc1pvxzxtchBj78hJP6Cf5TCMFSXw+Fz5rF1dR23QDbN1mkHs7adr8GW4kSWqU7Q7NDwfIrJJtO7Hi42GyXtvEONHbiRPOe8stqUly7MvUoN+5kfjBM8Qqpfl2+FNhTYWpMfYdPUnE7u536WqzFmsaqJctz3gBxH9Ex7dFtrxR4qiqEr9Qtlu3xGn7Bw07/+i1D+ey3ONkZLN+LQ714cgj8fRS4Hj29SCmXp5Kt5/82cD/VN3NtHw== smoser@brickies
```

[https://cloudinit.readthedocs.io/en/latest/topics/examples.html](https://cloudinit.readthedocs.io/en/latest/topics/examples.html)

## D’autres petites subtilités

Des images par défaut peuvent être utilisées :

```bash
$ multipass find
Image                       Aliases           Version          Description
snapcraft:core18            18.04             20201111         Snapcraft builder for Core 18
snapcraft:core20            20.04             20210921         Snapcraft builder for Core 20
snapcraft:devel                               20220514         Snapcraft builder for the devel series
core                        core16            20200818         Ubuntu Core 16
core18                                        20211124         Ubuntu Core 18
18.04                       bionic            20220513         Ubuntu 18.04 LTS
20.04                       focal,lts         20220505         Ubuntu 20.04 LTS
21.10                       impish            20220309         Ubuntu 21.10
22.04                       jammy             20220506         Ubuntu 22.04 LTS
daily:22.10                 devel,kinetic     20220518         Ubuntu 22.10
appliance:adguard-home                        20200812         Ubuntu AdGuard Home Appliance
appliance:mosquitto                           20200812         Ubuntu Mosquitto Appliance
appliance:nextcloud                           20200812         Ubuntu Nextcloud Appliance
appliance:openhab                             20200812         Ubuntu openHAB Home Appliance
appliance:plexmediaserver                     20200812         Ubuntu Plex Media Server Appliance
anbox-cloud-appliance                         latest           Anbox Cloud Appliance
charm-dev                                     latest           A development and testing environment for charmers
docker                                        latest           A Docker environment with Portainer and related tools
minikube                                      latest           minikube is local Kubernetes
```

## Communication avec WSL | Windows

Initialement, j’avais écris l’article pour l’utilisation avec Windows. Entre temps j’ai changé d’Os et je n’ai pas retesté sous Microsoft Windows.

Je remets mes notes ici concernant cet OS.

Dans le cadre où vous souhaitez communiquer et échanger avec wsl  :

Commande : `Get-NetIPInterface | where {$_.InterfaceAlias -eq "vEthernet (WSL)" -or $_.InterfaceAlias -eq "vEthernet (Default Switch)" } | Set-NetIPInterface -Forwarding Enabled`

Ceci est une courte introduction à Multipass, Next-Inpact à réalisé un très bon article là dessus : [https://www.nextinpact.com/article/30316/107156-multipass-creez-et-gerer-simplement-machines-virtuelles-ubuntu-depuis-linux-macos-ou-windows](https://www.nextinpact.com/article/30316/107156-multipass-creez-et-gerer-simplement-machines-virtuelles-ubuntu-depuis-linux-macos-ou-windows)

## Sources :

[https://multipass.run/](https://multipass.run/)

[https://wsl.dev/swarmenetes/](https://wsl.dev/swarmenetes/)

[https://techcommunity.microsoft.com/t5/itops-talk-blog/windows-subsystem-for-linux-2-addressing-traffic-routing-issues/ba-p/1764074](https://techcommunity.microsoft.com/t5/itops-talk-blog/windows-subsystem-for-linux-2-addressing-traffic-routing-issues/ba-p/1764074)

[https://blah.cloud/infrastructure/using-cloud-init-for-vm-templating-on-vsphere/](https://blah.cloud/infrastructure/using-cloud-init-for-vm-templating-on-vsphere/)

[https://cloudinit.readthedocs.io/en/latest/index.html](https://cloudinit.readthedocs.io/en/latest/index.html)

[https://www.vagrantup.com/docs/cloud-init](https://www.vagrantup.com/docs/cloud-init)

[https://learn.hashicorp.com/tutorials/terraform/cloud-init](https://learn.hashicorp.com/tutorials/terraform/cloud-init)
