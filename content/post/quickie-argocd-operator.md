---
date: 2022-08-24T05:11:00
description: "Quickie - ArgoCd Operator and namespace locking "
tags: ["Quickie","namespace","ArgoCd","Operator","Pod","Kubernetes"]
title: "Quickie : Problème de namespace lors de la publication d’application via argo-cd operator."
thumbnailImagePosition: left
thumbnailImage: https://imgur.com/MsBxDTh.png
---
Lors de l’installation de l’opérateur ArgoCd, [artifacthub.io](http://artifacthub.io) nous propose une commande pour l’installation : `kubectl create -f [https://operatorhub.io/install/alpha/argocd-operator.yaml](https://operatorhub.io/install/alpha/argocd-operator.yaml)`

`<!--more-->`

```yaml
# [argocd-operator.yaml](https://operatorhub.io/install/alpha/argocd-operator.yaml)
---
apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
  name: my-argocd-operator
  namespace: operators
spec:
  channel: alpha
  name: argocd-operator
  source: operatorhubio-catalog
  sourceNamespace: olm

#argocd.yaml 
---
apiVersion: argoproj.io/v1alpha1
kind: ArgoCD
metadata:
  name: thunder-argocd
spec: {}
```

La problématique :

Néanmoins, lors d’un déploiement sur un autre *namespace* sur lequel l’application ArgoCd est déployé est impossible, il affiche une erreur :

## Côté applicatif

![Untitled](https://i.imgur.com/MsBxDTh.png)

Je me dis “ Arf! bon c’est un paramètre qui est mal rempli, je vais voir dans les *settings>clusters*”.

J’ajoute les namespaces à gérer.

## Côté paramètre du cluster

![Untitled](https://i.imgur.com/pKNkyNm.png)

Et….

![Untitled](https://i.imgur.com/geajmoN.png)

**Comment pouvoir déployer un applicatif en dehors de son propre namespace ?**

Voici une des solutions :

⇒ Lors de l’application de la subscription : 

```yaml
#[argocd-operator.yaml](https://operatorhub.io/install/alpha/argocd-operator.yaml)
---
apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
	name: my-argocd-operator
  namespace: operators
spec:
  channel: alpha
  config:
    env:
      - name: ARGOCD_CLUSTER_CONFIG_NAMESPACES
        value: '*'
      - name: DISABLE_DEFAULT_ARGOCD_INSTANCE
        value: 'true'
  name: argocd-operator
  source: operatorhubio-catalog
  sourceNamespace: olm

#argocd.yaml 
---
apiVersion: argoproj.io/v1alpha1
kind: ArgoCD
metadata:
  name: thunder-argocd
spec: {}
```

⇒ Suppression et redéploiement de l’application :

![Untitled](https://i.imgur.com/XtwcK8G.png)

![Untitled](https://i.imgur.com/by3eESc.png)

Comme on peut le voir, tout semble ok.

Source :

[https://github.com/argoproj-labs/argocd-operator/issues/523](https://github.com/argoproj-labs/argocd-operator/issues/523)

Enjoy!
